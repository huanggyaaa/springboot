###### 知识点

1. 多个配置文件优先级: bootstrap.yml > application.yml > application.properties
2. classpath/config 和 classpath 根目录相比, classpath/config 下的配置文件优先级更高
3. @PropertySource("classpath:prop.properties") 读取指定目录下的配置文件
4. @Value("xxx") 只能使用 String 接收

###### 读取系统配置文件内容

* 配置文件定义, 在生效的配置文件中定义的才会生效

```properties
# 使用 application-dev.properties 配置文件
spring.profiles.active=dev

# dev 中指定的端口是 8090, 这里指定的 8089 不会生效
server.port=8089
```

* 获取

```java
@Component
public class SystemProperties {

    @Value("${server.port}")
    private String port;

    void getPort(){
        System.out.println(port);
    }

}
```

###### 读取自定义配置文件内容

* 创建 prop.properties 配置文件

```properties
web.file.path=/usr/local/
```

* 获取 @PropertySource("classpath:prop.properties"), 当前类读取 resources 目录下的 prop.properties 文件的值

```java
@Component
@PropertySource("classpath:prop.properties")
public class CustomProperties {

    @Value("${web.file.path}")
    private String path;

    void getPropertiesValue(){
        System.out.println(path);
    }
}
```
