package com.study;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 读取指定目录下的配置文件 --- @PropertySource("classpath:prop.properties")
 *
 * @author Huang
 * @date 2019/3/25 21:38
 **/
@Component
@PropertySource("classpath:prop.properties")
public class CustomProperties {

    @Value("${web.file.path}")
    private String path;

    void getPropertiesValue() {
        System.out.println(path);
    }
}
