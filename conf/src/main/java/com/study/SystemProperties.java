package com.study;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 读取系统配置文件
 *
 * @author Huang
 * @date 2019/3/25 21:40
 **/
@Component
public class SystemProperties {

    /**
     * 配置文件
     */
    @Value("${server.port}")
    private String port;

    void getPort() {
        System.out.println(port);
    }

}
