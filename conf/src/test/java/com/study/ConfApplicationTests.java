package com.study;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT) // 解决在测试环境中获取端口号是 -1
public class ConfApplicationTests {

    @Autowired
    private CustomProperties customProperties;
    @Autowired
    private SystemProperties systemProperties;

    @Test
    public void test() {
        customProperties.getPropertiesValue();
    }

    @Test
    public void test2() {
        systemProperties.getPort();
    }

}

