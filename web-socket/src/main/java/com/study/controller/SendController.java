package com.study.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huang
 * @date 2019/2/27 16:24
 **/
@RestController
@RequestMapping("/socket")
public class SendController {

    @GetMapping("/doSend/{message}")
    public void doSend(@PathVariable String message) {
        WebSocketServer.doSend(message);
    }
}
