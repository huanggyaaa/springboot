package com.study.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/17 15:45
 */
public class IniRealmTest {

    @Test
    public void testAuthentication(){

        // realm
        IniRealm iniRealm = new IniRealm("classpath:shiro.ini");

        // SecurityManager，并指定 realm
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager(iniRealm);
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();

        // 登陆
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("li4", "456");
        subject.login(usernamePasswordToken);

        System.out.println("是否具有 root 角色：" + subject.hasRole("root"));

        System.out.println("是否有 addProduct 权限：" + subject.isPermitted("addProduct"));
        // 和 isPermitted 功能一样，也是检查是否具有某权限，一个用返回值 true 和 false 来判断，一个看是否抛出异常来判断
        // subject.checkPermissions("addProduct");

        System.out.println("是否有 addOrder 权限：" + subject.isPermitted("addOrder"));

    }
}
