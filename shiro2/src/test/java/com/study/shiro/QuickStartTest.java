package com.study.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;

/**
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/17 15:04
 */
public class QuickStartTest {

    private final SimpleAccountRealm accountRealm = new SimpleAccountRealm();
    private final DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();


    @Before
    public void init(){

        // 添加用户并指定角色
        accountRealm.addAccount("marry", "123", "root", "admin");
        accountRealm.addAccount("milk", "456", "user");

        // 构建环境，设置 realm（设置数据源）
        defaultSecurityManager.setRealm(accountRealm);
    }

    @Test
    public void testAuthentication(){
        SecurityUtils.setSecurityManager(defaultSecurityManager);

        Subject subject = SecurityUtils.getSubject();

        // 用户输入的用户名和密码
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("marry", "123");
        subject.login(usernamePasswordToken);

        // 登陆结果，true 表示登陆成功（账号密码正确）
        System.out.println("登陆结果：" + subject.isAuthenticated());

        // true：有；false：没有
        System.out.println("是否具有 root 角色：" + subject.hasRole("root"));

        // 和上面功能一样，如果没有会抛出 UnauthorizedException 异常
        subject.checkRole("admin");

        System.out.println("当前登陆用户名：" + subject.getPrincipal());

    }
}
