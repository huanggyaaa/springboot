package com.study.shiro;

import com.study.shiro.config.CustomerRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;

/**
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/17 16:43
 */
public class CustomerRealmTest {

    private final CustomerRealm realm = new CustomerRealm();
    private final DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();

    @Before
    public void init(){
        defaultSecurityManager.setRealm(realm);
        SecurityUtils.setSecurityManager(defaultSecurityManager);
    }

    @Test
    public void testAuthentication(){
        Subject subject = SecurityUtils.getSubject();
        // 登陆
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("zhang3", "123");
        subject.login(usernamePasswordToken);

        System.out.println("登陆结果：" + subject.isAuthenticated());
    }
}
