package com.study.shiro.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * user
 * @author 
 */
@Data
public class User implements Serializable {
    private Long id;

    private String name;

    private String password;

    private String salt;

    /**
     * 用户对应角色列表
     */
    private List<Role> roles;

    private static final long serialVersionUID = 1L;
}