package com.study.shiro.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * permission
 * @author 
 */
@Data
public class Permission implements Serializable {
    private Long id;

    private String name;

    private static final long serialVersionUID = 1L;
}