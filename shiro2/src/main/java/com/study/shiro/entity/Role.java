package com.study.shiro.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * role
 * @author 
 */
@Data
public class Role implements Serializable {
    private Long id;

    private String name;

    private static final long serialVersionUID = 1L;

    /**
     * 角色对应权限列表
     */
    private List<Permission> perms;
}