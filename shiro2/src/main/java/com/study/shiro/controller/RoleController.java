package com.study.shiro.controller;

import com.study.shiro.common.RestResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/24 15:24
 */
@RestController
@RequestMapping("/role")
public class RoleController {
    @GetMapping("/admin/user")
    public RestResult<Object> adminUser(){
        return RestResult.success("用户接口，需要 role 角色才能访问");
    }
}
