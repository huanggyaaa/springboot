package com.study.shiro.controller;

import com.study.shiro.common.RestResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/24 15:11
 */
@RestController
@RequestMapping("/authc")
public class AuthcController {

    @GetMapping("/order")
    public RestResult<Object> order(){
        return RestResult.success("订单接口，需要登陆才能访问");
    }
}
