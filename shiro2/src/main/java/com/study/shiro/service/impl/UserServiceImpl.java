package com.study.shiro.service.impl;

import com.study.shiro.dao.UserDAO;
import com.study.shiro.entity.User;
import com.study.shiro.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/18 16:46
 */
@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private UserDAO userDAO;

    @Override
    public User selectByName(String name) {
        return userDAO.selectByName(name);
    }

    @Override
    public User selectByNameAndPassword(String name, String pwd) {
        return userDAO.selectByNameAndPassword(name, pwd);
    }

    @Override
    public User listRoleAndPermsByUserId(Long userId) {
        return userDAO.listRoleAndPermsByUserId(userId);
    }

    @Override
    public User listRoleAndPermsByUsername(String username) {
          return userDAO.listRoleAndPermsByUsername(username);
    }
}
