package com.study.shiro.service;

import com.study.shiro.entity.User;

/**
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/18 9:58
 */
public interface IUserService {
    User selectByName(String name);

    User selectByNameAndPassword(String name, String pwd);

    /**
     * 用户对应的角色和权限列表
     */
    User listRoleAndPermsByUserId(Long userId);

    /**
     * 用户对应的角色和权限列表
     */
    User listRoleAndPermsByUsername(String username);
}
