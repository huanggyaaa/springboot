package com.study.shiro.dao;

import com.study.shiro.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserDAO {
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    @Select("select * from user where name = #{name}")
    User selectByName(String name);

    @Select("select * from user where name = #{name} ane password = #{pwd}")
    User selectByNameAndPassword(@Param("name") String name,
                                 @Param("pwd") String pwd);

    /**
     * 用户对应的角色和权限列表
     */
    User listRoleAndPermsByUserId(Long userId);

    /**
     * 用户对应的角色和权限列表
     */
    User listRoleAndPermsByUsername(@Param("username") String username);

}