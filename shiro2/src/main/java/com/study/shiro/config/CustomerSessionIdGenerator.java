package com.study.shiro.config;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;

import java.io.Serializable;
import java.util.UUID;

/**
 * 自定义 shiro-session ID 生成策略
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/25 11:49
 */
public class CustomerSessionIdGenerator extends JavaUuidSessionIdGenerator {

    @Override
    public Serializable generateId(Session session) {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
