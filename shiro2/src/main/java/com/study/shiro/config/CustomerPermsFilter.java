package com.study.shiro.config;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * 自定义权限过滤器
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/24 16:31
 */
public class CustomerPermsFilter extends PermissionsAuthorizationFilter {

    @Override
    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws IOException {

        Subject subject = getSubject(request, response);
        String[] perms = (String[]) mappedValue;

        if (perms != null && perms.length > 0) {
            Set<String> permArr = CollectionUtils.asSet(perms);
            for (String perm : permArr) {
                if (subject.isPermitted(perm)){
                    return true;
                }
            }
            return false;
        }

        return true;
    }
}
