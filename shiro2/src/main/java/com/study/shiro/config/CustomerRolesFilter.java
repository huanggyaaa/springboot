package com.study.shiro.config;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.util.Set;

/**
 * 自定义角色过滤器
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/24 16:25
 */
public class CustomerRolesFilter extends AuthorizationFilter {

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        Subject subject = super.getSubject(servletRequest, servletResponse);
        String[] rolesArray = (String[]) o;
        if (rolesArray != null && rolesArray.length != 0) {
            Set<String> roles = CollectionUtils.asSet(rolesArray);
            for (String role : roles) {
                if (subject.hasRole(role)){
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

}
