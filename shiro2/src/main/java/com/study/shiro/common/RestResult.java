package com.study.shiro.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一响应
 * @author huang guang yue
 * @version v1.0.0
 * @date 2020 2020/10/29 11:05
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RestResult<T> {

    private Integer code;

    private String msg;

    private T data;

    public static <T> RestResult<T> success() {
        return new RestResult<>(0, "请求成功", null);
    }

    public static <T> RestResult<T> success(T data) {
        return new RestResult<>(0, "请求成功", data);
    }

    public static <T> RestResult<T> success(String msg) {
        return new RestResult<>(0, msg, null);
    }

    public static <T> RestResult<T> success(String msg, T t) {
        return new RestResult<>(0, msg, t);
    }

    public static <T> RestResult<T> error() {
        return new RestResult<>(1, "请求失败", null);
    }

    public static <T> RestResult<T> error(String msg) {
        return new RestResult<>(1, msg, null);
    }

    public static <T> RestResult<T> error(Integer code, String msg) {
        return new RestResult<>(code, msg, null);
    }

    public static <T> RestResult<T> error(String msg, T t) {
        return new RestResult<>(1, msg, t);
    }
}
