# shiro web
### 过滤器
+ anon：公开资源，不需要登陆就能访问
+ authc：登陆后才能访问
+ roles：必须有某个角色才能访问。roles["admin,user"] 表示***同时具有*** admin 和 user 才能访问。这是官方的，后期可以自定义过滤器，具有其中一个角色就能访问
+ perms：必须具有某个权限才能访问。同上 prems["addProduct,deleteProduct"] 也是必须同时具有才能访问。
+ logout：退出时清除 session 然后后跳转到指定的 url
+ port：端口拦截器，指定的端口才能访问
+ ssl：只能 https 才能访问
### 访问路径
+ `/user?`  单级匹配一个字符。匹配 /user1, 不匹配 /user/
+ `/user*`  单级匹配不限个数字符串。匹配 /userAdd, 不匹配 /user1
+ `/user/**` 多级匹配不限个数字符串。匹配 /user/add, 匹配 /user/manage/add
### 命中
`/user/** = filter1` 和 `/user/add = filter2`。url 为 /user/add 究竟是命中 filter1 还是 filter2 呢？<br>
答案是顺序，哪个先匹配成功就使用哪个过滤器，就不往下走了
### 缓存
+ 默认对登陆关闭，对授权开启，因为相对权限的数据要复杂且多一些。可以自定义。***注意不是 session***
### Session
+ 和 web session 类似，区分不同的客户端连接
+ shiro 有一个管理 session 的工具，叫 SessionManager，可以自定义。通过 SessionDAO 进行持久化存储。可以存储在 数据库、redis、内存等
  
+ 依赖
```xml
<dependencies>
    <!-- springboot-web -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-redis</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <!-- mybatis -->
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>2.1.4</version>
    </dependency>
    <!-- shiro -->
    <dependency>
        <groupId>org.apache.shiro</groupId>
        <artifactId>shiro-spring-boot-web-starter</artifactId>
        <version>1.4.0</version>
    </dependency>
    <!-- spring 整合 shiro -->
    <dependency>
        <groupId>org.apache.shiro</groupId>
        <artifactId>shiro-spring</artifactId>
        <version>1.4.0</version>
    </dependency>
    <!-- mysql，注释掉 scope -->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <!--<scope>runtime</scope>-->
    </dependency>
    <!-- druid 数据源 -->
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid</artifactId>
        <version>1.1.6</version>
    </dependency>
    <!-- lombok -->
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <optional>true</optional>
    </dependency>
    <!-- 测试 -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
        <exclusions>
            <exclusion>
                <groupId>org.junit.vintage</groupId>
                <artifactId>junit-vintage-engine</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13.1</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```
