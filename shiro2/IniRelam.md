# shiro学习
### QuickStart
+ 依赖
```xml
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring-boot-web-starter</artifactId>
    <version>1.4.0</version>
</dependency>
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring</artifactId>
    <version>1.4.0</version>
</dependency>
```
+ 配置文件
```ini
# 定义用户 用户名=密码,角色1,角色2,...角色N
[users]
# 用户名 zhang3  密码是 123， 角色是 admin 和 root
zhang3 = 123, admin, root
# 用户名 li4  密码是 456， 角色是 productManager 产品经理
li4 = 456, productManager


# 定义角色 角色名=权限1,权限2,...权限N
[roles]
# 管理员什么都能做
admin = *
root = *
# 产品经理只能做产品管理，下面两种写法效果一样；一种是模块：动作；一种是具体操作
# productManager = product:add, product:delete, product:update, product:list
# productManager = product:*
productManager = addProduct,deleteProduct,editProduct,updateProduct,listProduct
# 订单经理只能做订单管理
orderManager = addOrder,deleteOrder,editOrder,updateOrder,listOrder
```
+ 测试
```java
package com.study.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * @author huang guang yue
 * @version v1.0.0
 * @date 2021 2021/3/17 15:45
 */
public class IniRealmTest {

    @Test
    public void testAuthentication(){

        // realm
        IniRealm iniRealm = new IniRealm("classpath:shiro.ini");

        // SecurityManager，并指定 realm
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager(iniRealm);
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();

        // 登陆
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("li4", "456");
        subject.login(usernamePasswordToken);

        System.out.println("是否具有 root 角色：" + subject.hasRole("root"));

        System.out.println("是否有 addProduct 权限：" + subject.isPermitted("addProduct"));
        // 和 isPermitted 功能一样，也是检查是否具有某权限，一个用返回值 true 和 false 来判断，一个看是否抛出异常来判断
        // subject.checkPermissions("addProduct");

        System.out.println("是否有 addOrder 权限：" + subject.isPermitted("addOrder"));

    }
}
```