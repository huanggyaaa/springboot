package com.study.entity.elasticsearch;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * @author huang
 */
@Data
@Document(indexName = "test_db", type = "t_user")
public class User implements Serializable {
    @Id
    private String id;

    private String username;

    private String password;

    private Date createTime;

    private Integer age;
}
