package com.study.entity.elasticsearch;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;

/**
 * @author huang
 */
@Data
@Document(indexName = "test_db1", type = "t_player")
public class Player {
    @Id
    private Long id;

    private String name;

    private Integer age;

    private BigDecimal salary;

    private String team;

    private String position;

    public Player(Long id, String name, Integer age, BigDecimal salary, String team, String position) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.team = team;
        this.position = position;
    }
}
