package com.study.repository;

import com.study.entity.elasticsearch.Player;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author huang
 */
public interface PlayerRepository extends ElasticsearchRepository<Player, Long> {
}
