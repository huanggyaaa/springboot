package com.study.repository;

import com.study.entity.elasticsearch.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author huang
 */
public interface UserRepository extends ElasticsearchRepository<User, String> {
}
