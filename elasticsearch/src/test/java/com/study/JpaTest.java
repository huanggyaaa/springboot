package com.study;

import com.study.entity.elasticsearch.User;
import com.study.repository.PlayerRepository;
import com.study.repository.UserRepository;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * jpa 测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JpaTest {

    @Resource
    private UserRepository userRepository;
    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;

    private User user;

    private List<User> list = new ArrayList<>();

    @Before
    public void setValue() {
        user = new User();
        user.setId("123123123");
        user.setPassword("123456");
        user.setUsername("Marry");

        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setId("ID" + i);
            user.setPassword("pwd" + i + i + i + i);
            user.setUsername("Marry" + i);
            user.setAge(16 + i);
            ZoneId zoneId = ZoneId.systemDefault();
            LocalDate localDate = LocalDate.now().plusDays(i);
            ZonedDateTime zdt = localDate.atStartOfDay(zoneId);
            user.setCreateTime(Date.from(zdt.toInstant()));
            list.add(user);
        }
    }

    @Test
    public void t() {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.mustNot(QueryBuilders.existsQuery("password.keyword"));
        Iterable<User> search = userRepository.search(boolQueryBuilder);
        System.out.println("1");
    }

    @Test
    public void test() {
        // 没有就新增, 有就修改
        userRepository.save(user);
        // 通过 ID 查询
        User user = userRepository.findById("123123123").get();
        // 通过 IO 删除
        userRepository.deleteById("123123123");
        // 通过实体删除
        userRepository.delete(user);
        // 查询所有
        Iterable<User> all = userRepository.findAll();
        // 删除全部
        userRepository.deleteAll();
        // 是否存在
        boolean b = userRepository.existsById("123123123");
        // 总数
        long count = userRepository.count();
        // 批量保存
        Iterable<User> users = userRepository.saveAll(list);
        // 批量删除
        userRepository.deleteAll(list);
        // 排序查询
        Iterable<User> list = userRepository.findAll(new Sort(Sort.Direction.DESC, "createTime"));
        // 分页查询
        Pageable pageable = PageRequest.of(1, 10);
        Page<User> all1 = userRepository.findAll(pageable);
        // 分页和排序
        Pageable pageable1 = PageRequest.of(1, 10, new Sort(Sort.Direction.DESC, "createTime"));
        Page<User> all2 = userRepository.findAll(pageable1);

        // 搜索
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        // java Date 数据在 es 中是 timestamp
        long start = LocalDateTime.now().minusDays(2).toInstant(ZoneOffset.of("+8")).toEpochMilli();
        long end = LocalDateTime.now().plusDays(3).toInstant(ZoneOffset.of("+8")).toEpochMilli();
        // 时间范围, 2 天前 <= createTime <= 3 天后
        boolQueryBuilder.must(QueryBuilders.rangeQuery("createTime").gte(start).lte(end));
        // 整数范围, 18 < age
        boolQueryBuilder.must(QueryBuilders.rangeQuery("age").gt(18));
        // 字段查询, username == Marry && username != Milk (termQuery 值如果是字符串, 需要拼接 .keyword)
        boolQueryBuilder.must(QueryBuilders.termQuery("username.keyword", "Marry"))
                .mustNot(QueryBuilders.termQuery("username.keyword", "Milk"));
        // in 查询, username 是 Bob 或者 Rose(termsQuery 和 termQuery 区别是 termsQuery 可以多值匹配)
        boolQueryBuilder.must(QueryBuilders.termsQuery("username.keyword", "Bob", "Rose"));
        // 模糊查询
        boolQueryBuilder.must(QueryBuilders.wildcardQuery("username", String.format("*%s*", "rry")));
        // 前缀查询, 以 Mar 开头的(值是字符串要加 .keyword )
        boolQueryBuilder.must(QueryBuilders.prefixQuery("username.keyword", "Mar"));
        // 分词查询, 有限公司分为 '有限' 和 '公司', 所以 '有限企业', '大公司' 也能查出来(未测试, 测试环境没安装分词器)
        boolQueryBuilder.must(QueryBuilders.matchQuery("companyName", "有限公司"));
        // 和 matchQuery 的区别是: 不会分词. 类似模糊查询了, 只能查询包含 '有限公司'的数据(未测试, 测试环境没安装分词器)
        boolQueryBuilder.must(QueryBuilders.matchPhraseQuery("companyName", "有限公司"));
        // 查询
        Iterable<User> users1 = userRepository.search(boolQueryBuilder);
        // 查询并分页
        Page<User> users2 = userRepository.search(boolQueryBuilder, pageable1);


        // elasticsearchTemplate 查询
        NativeSearchQuery query = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder).withPageable(pageable1).build();
        List<User> users3 = elasticsearchTemplate.queryForList(query, User.class);
    }

}
