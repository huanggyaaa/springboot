package com.study;

import com.study.Utils.ESAggregationUtil;
import com.study.entity.elasticsearch.Player;
import com.study.repository.PlayerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayerTest {

    @Resource
    private PlayerRepository playerRepository;
    @Resource
    private ESAggregationUtil esAggregationUtil;

    @Test
    public void insert() {
        ArrayList<Player> players = new ArrayList<>();
        players.add(new Player(1L, "Marry", 20, new BigDecimal("5999"), "cav", "beijing"));
        players.add(new Player(2L, "Rose", 30, new BigDecimal("3999"), "cav", "shanghai"));
        players.add(new Player(3L, "July", 42, new BigDecimal("4999"), "war", "shanghai"));
        players.add(new Player(4L, "HanMeimei", 29, new BigDecimal("5999"), "war", "beijing"));
        players.add(new Player(5L, "Bob", 19, new BigDecimal("6999"), "war", "chengdu"));
        players.add(new Player(6L, "Milk", 34, new BigDecimal("7999"), "tim", "chengdu"));
        players.add(new Player(7L, "Jack", 37, new BigDecimal("3999"), "tim", "chengdu"));
        players.add(new Player(8L, "Jim", 34, new BigDecimal("1999"), "tim", "beijing"));
        players.add(new Player(9L, "Green", 33, new BigDecimal("4999"), "tim", "wuhan"));
        playerRepository.saveAll(players);
    }

    @Test
    public void groupBy() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("team.keyword");
        esAggregationUtil.groupBy("test_db1", "t_player", strings, 10);
    }
}
