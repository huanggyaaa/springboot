package com.study.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * 上下文监听器, 用于项目启动加载缓存数据等, 使用线程防止阻塞项目启动
 */
@WebListener
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("加载缓存数据, com.study.listener.ContextListener");
    }

}
