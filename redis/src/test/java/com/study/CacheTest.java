package com.study;

import com.study.entity.User;
import com.study.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * 缓存操作
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CacheTest {
    @Autowired
    private IUserService userService;

    /**
     * 测试数据插入, 会放进缓存
     */
    @Test
    public void test1() {
        User user = new User();
        user.setUsername("Marry");
        user.setBorn(new Date());
        user.setGender("女");
        user.setId(1);
        userService.insertSelective(user);
    }

    /**
     * 测试数据查询, 是否走缓存
     */
    @Test
    public void test2() {
        userService.selectByPrimaryKey(1);
    }

    /**
     * 测试数据更新
     */
    @Test
    public void test3() {
        // 数据更新, 会同步更新缓存
        User user = new User();
        user.setId(1);
        user.setUsername("Rose");
        userService.updateByPrimaryKeySelective(user);
        // 测试缓存是否更新
        userService.selectByPrimaryKey(1);
    }

    /**
     * 测试删除所有缓存
     */
    @Test
    public void test4() {
        // 删除所有缓存
        userService.clearCache();
        // 测试是否走 mysql
        //userService.selectByPrimaryKey(1);
    }

    /**
     * 测试删除缓存
     */
    @Test
    public void test5() {
        // 会同步删除缓存
        userService.deleteByPrimaryKey(1);

        // 再次查询会到 mysql , 结果是 null
        userService.selectByPrimaryKey(1);
    }


}
