package com.study;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 原生操作
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisApplicationTests {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * redisTemplate 存储会乱码, 需要自定义转码, 具体配置在启动类
     */
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 基本操作
     */
    @Test
    public void contextLoads() {

        // 添加
        stringRedisTemplate.opsForValue().set("dgg_test_h1", "1");

        // 检测 key 是否存在
        Boolean dgg_test_h11 = stringRedisTemplate.hasKey("dgg_test_h1");
        System.out.println("dgg_test_h1存在？" + dgg_test_h11);

        // 带过期时间添加，1 分钟后过期
        stringRedisTemplate.opsForValue().set("dgg_test_h2", "2", 3600, TimeUnit.SECONDS);

        // 获取
        String dgg_test_h1 = stringRedisTemplate.opsForValue().get("dgg_test_h1");
        System.out.println("dgg_test_h1：" + dgg_test_h1);

        // 设置过期时间
        stringRedisTemplate.expire("dgg_test_h1", 7200, TimeUnit.SECONDS);

        // 获取 key 的过期时间，第二个参数表示换算成哪种计数方式
        Long expire = stringRedisTemplate.getExpire("dgg_test_h1", TimeUnit.SECONDS);
        System.out.println("dgg_test_h1 过期时间：" + expire);

        // age 加 1
        stringRedisTemplate.boundValueOps("age").increment(1);
        // time 减 1
        stringRedisTemplate.boundValueOps("time").increment(-1);

        // 删除 key，刚才添加的全部删除
        stringRedisTemplate.delete("01dgg_test_h1");
        stringRedisTemplate.delete("dgg_test_h1");
        stringRedisTemplate.delete("dgg_test_h2");
        stringRedisTemplate.delete("age");
        stringRedisTemplate.delete("time");

    }

    /**
     * list 操作
     */
    @Test
    public void listTest() {

        // 从右边添加（尾部追加）
        stringRedisTemplate.opsForList().rightPush("myList", "3");
        stringRedisTemplate.opsForList().rightPush("myList", "4");
        stringRedisTemplate.opsForList().rightPush("myList", "5");

        // 从左边添加（头部添加）
        stringRedisTemplate.opsForList().leftPush("myList", "2");
        stringRedisTemplate.opsForList().leftPush("myList", "1");

        // 添加 List
        List<String> list = new ArrayList<>();
        list.add("6");
        list.add("7");
        list.add("8");
        stringRedisTemplate.opsForList().rightPushAll("myList", list);

        // 获取当前 list 所有元素
        List<String> myList = stringRedisTemplate.opsForList().range("myList", 0, -1);
        assert myList != null;
        for (String s : myList) {
            System.out.println(s);
        }

        // 获取前 5 个元素，前闭后开
        stringRedisTemplate.opsForList().range("myList", 0, 5);

        // 删除所有值是 1 的元素
        stringRedisTemplate.opsForList().remove("myList", 0, "1");

    }

    /**
     * hash 操作
     */
    @Test
    public void hashTest() {

        // 添加
        HashMap<String, Object> map = new HashMap<>();
        map.put("userId", "1002");
        map.put("username", "张三");
        map.put("sex", "0");
        map.put("born", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        stringRedisTemplate.opsForHash().putAll("user:" + map.get("userId"), map);

        // 添加一个字段, password
        stringRedisTemplate.opsForHash().put("user:" + map.get("userId"), "password", "111111");

        // 获取 hash
        Map<Object, Object> user = stringRedisTemplate.opsForHash().entries("user:" + map.get("userId"));
        System.out.println("用户信息：" + user);

        // 获取 hash 的一个字段
        String username = (String) stringRedisTemplate.opsForHash().get("user:" + map.get("userId"), "username");
        System.out.println("用户姓名：" + username);

        // 获取所有 key
        Set<Object> keys = stringRedisTemplate.opsForHash().keys("user:" + map.get("userId"));
        System.out.println("keys：" + keys);

        // 获取所有 value
        List<Object> values = stringRedisTemplate.opsForHash().values("user:" + map.get("userId"));
        System.out.println(values);

    }

}

