package com.study.service;

import com.study.entity.User;

import java.util.List;

public interface IUserService {
    int deleteByPrimaryKey(Integer id);

    User insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    User updateByPrimaryKeySelective(User record);

    List<User> listUser();

    void clearCache();

}
