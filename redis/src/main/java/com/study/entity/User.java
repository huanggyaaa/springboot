package com.study.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * @author huang
 */
@Data
public class User implements Serializable {

    private Integer id;

    private String gender;

    private String username;

    private Date born;

}