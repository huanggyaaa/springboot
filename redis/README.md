###### 配置文件

```properties
# mysql
spring.datasource.url=jdbc:mysql://172.16.22.33:3306/test?useUnicode=true&characterEncoding=utf8
spring.datasource.username =root
spring.datasource.password =123456

# mybatis
mybatis.mapperLocations=classpath:mapping/*.xml
mybatis.configuration.log-impl=org.apache.ibatis.logging.stdout.StdOutImpl
mybatis.type-aliases-package=com.study.entity
mybatis.configuration.map-underscore-to-camel-case=true

# 指定缓存方式
spring.cache.type=redis

# redis 单机
# 第 15 个分片, index 是 14 , 从 0 开始
spring.redis.database=14
spring.redis.host=172.16.22.33
spring.redis.port=6379
spring.redis.timeout=1000
```

###### 启动类

```java
@EnableCaching  // 开启缓存支持
@MapperScan(basePackages = "com.study.mapper", annotationClass = Repository.class)
@SpringBootApplication
public class RedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class, args);
    }
}
```

###### redis 配置类

```java
@Configuration
public class RedisConfig {

    /**
     * 自定义 key, 使用第一个参数作为 key
     *
     *  使用方式: @Cacheable(keyGenerator = "firstParamKeyGenerator")
     */
    @Bean(name = "firstParamKeyGenerator")
    public KeyGenerator firstParamKeyGenerator(){
        return (target, method, params) -> params[0].toString();
    }

    /**
     * 自定义 key, list 作为 key
     *
     *  使用方式: @Cacheable(keyGenerator = "list")
     */
    @Bean(name = "list")
    public KeyGenerator list(){
        return (target, method, params) -> "list";
    }

    /**
     * cache 乱码
     */
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        RedisSerializer<String> redisSerializer = new StringRedisSerializer();
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);

        // 配置序列化
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig();
        RedisCacheConfiguration redisCacheConfiguration = config.serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(redisSerializer))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(jackson2JsonRedisSerializer));

        return RedisCacheManager.builder(factory)
                .cacheDefaults(redisCacheConfiguration)
                .build();
    }

    /**
     * redisTemple 乱码
     */
    @Bean(name="redisTemplate")
    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory) {

        RedisTemplate<String, String> template = new RedisTemplate<>();

        RedisSerializer<String> redisSerializer = new StringRedisSerializer();

        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);

        template.setConnectionFactory(factory);
        //key序列化方式
        template.setKeySerializer(redisSerializer);
        //value序列化
        template.setValueSerializer(jackson2JsonRedisSerializer);
        //value hashmap序列化
        template.setHashValueSerializer(jackson2JsonRedisSerializer);

        return template;
    }
}
```

###### 业务层

```java
/**
 * @Cacheable 指定 key 前缀, redis-cache-user:: + 方法中的 key
 */
@CacheConfig(cacheNames = "redis-cache-user")
@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private TUserDAO userMapper;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @CacheEvict 删除缓存, key 默认是参数, 每次都执行
     */
    @Override
    @CacheEvict
    public int deleteByPrimaryKey(Integer id) {
        return userMapper.deleteByPrimaryKey(id);
    }

    /**
     * @CachePut 更新缓存, 每次都执行
     */
    @Override
    @CachePut(key = "#record.id")
    public User insertSelective(User record) {
        return userMapper.insertSelective(record) == 1 ? record : null;
    }

    /**
     * @Cacheable 开启缓存功能, 有就查询缓存, 没有就先查询 mysql 再放进缓存, 不一定执行
     */
    @Override
    @Cacheable
    public User selectByPrimaryKey(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    @CachePut(key = "#record.id")
    public User updateByPrimaryKeySelective(User record) {
        return userMapper.updateByPrimaryKeySelective(record) == 1 ? record : null;
    }

    /**
     * 使用自定义 key
     */
    @Override
    @Cacheable(keyGenerator = "list")
    public List<User> listUser() {
        return userMapper.listUser();
    }

    /**
     * 删除所有缓存
     */
    @Override
    @CacheEvict
    public void clearCache(){
        stringRedisTemplate.delete(stringRedisTemplate.keys("redis-cache-user:*"));
    }
}
```

###### 测试

+ RestTemplate 测试, [查看](./src/test/java/com/study/RedisApplicationTests.java)
+ Cache 测试, [查看](./src/test/java/com/study/CacheTest.java)
