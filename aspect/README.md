###### 概念

在指定的目标执行前后执行额外的操作, 比如记录日志等操作

| 目标类型        | 切点   |  描述  |
| ---------------| -----  | :----:  |
| 方法           | @Pointcut("execution(* find*(..))")   |find 打头的方法, 会匹配整个项目中所有的方法     |
| 应用了注解的方法 |    @Pointcut("@annotation(com.study.anno.UserAnno)")    |  整个项目中使用了 @UserAnno 注解的方法  |
| 类             |   @Pointcut("within(com.study.service.UserService1)")   |   这个类下面的所有方法   |
| 父类/父类接口    |   @Pointcut("this(com.study.service.UserDao)")   |    子类的所有方法  |
| 包             |    @Pointcut("within(com.study.service.impl..*)")    |  这个包下面的所有方法  |

###### 开发流程, 这里举例匹配注解

* 声明注解

```java
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.METHOD })
public @interface UserAnno {
}
```

* 切点和切片

```java
@Aspect
@Component
public class UserAspect {
     /**
     * 切点, 匹配注解
     */
    @Pointcut("@annotation(com.study.anno.UserAnno)")
    public void matchAnno(){}
    
    /**
     * 前置切片
     */
    @Before("matchAnno()")
    public void doBefore(JoinPoint point){
        // 获取目标方法参数
        Object[] args = point.getArgs();
        if (args != null && args.length > 0){
            // 参数为对象
            if (args[0] instanceof User){
                User user = (User) args[0];
                System.out.println("第一个参数：");
                System.out.println(user.toString());
            }
            // 基本数据类型参数
            if (args[1].getClass() == Long.class){
                System.out.println("第二个参数:" + args[1]);
            }
        }
    }
}
```

* 使用

```java
@Service
public class UserService {

    /**
     * 方法执行前触发切片
     */
    @UserAnno
    public User updateById(User user, Long id){
        User user1 = new User();
        user1.setId(124L);
        user1.setUsername("Marry");
        user1.setPassword("111111");
        return user1;
    }
}
```