package com.study;

import com.study.entity.User;
import com.study.service.*;
import com.study.service.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AspectApplicationTests {

    @Autowired
    private UserService4 userService4;

    @Autowired
    private UserService userService;

    @Autowired
    private UserService1 userService1;

    @Autowired
    private UserDao userService3;

    @Autowired
    private UserServiceImpl userServiceImpl;

    /**
     * 指定方法
     */
    @Test
    public void test() {
        userService4.findById();
    }

    /**
     * 注解
     */
    @Test
    public void test1() {
        userService.updateById(new User(), 1L);
    }

    /**
     * 指定类(该类下面所有方法都会触发通知)
     */
    @Test
    public void test2() {
        userService1.test2();
        userService1.test();
    }

    /**
     * 指定父类/接口(子类中所有方法会触发通知)
     */
    @Test
    public void test3() {
        userService3.deleteById(1L);
        userService3.getById();
    }

    /**
     * 指定父类/接口(子类中所有方法会触发通知)
     */
    @Test
    public void test4() {
        userServiceImpl.test();
    }

}
