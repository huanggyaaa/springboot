package com.study.service;

import com.study.anno.UserAnno;
import com.study.entity.User;
import org.springframework.stereotype.Service;

/**
 * @author huang
 * @date 2019/3/26 21:56
 **/
@Service
public class UserService {

    /**
     * 测试注解是否触发通知 --- @Pointcut("@annotation(com.study.anno.UserAnno)")
     */
    @UserAnno
    public User updateById(User user, Long id) {
        User user1 = new User();
        user1.setId(124L);
        user1.setUsername("Marry");
        user1.setPassword("111111");
        return user1;
    }


}
