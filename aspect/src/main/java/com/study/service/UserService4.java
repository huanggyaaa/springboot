package com.study.service;

import org.springframework.stereotype.Service;

/**
 * 测试 find 开头的方法是否触发通知 --- @Pointcut("execution(* find*(..))")
 *
 * @author Huang
 * @date 2019/3/26 22:28
 **/
@Service
public class UserService4 {

    public void findById() {
        System.out.println(111);
    }
}
