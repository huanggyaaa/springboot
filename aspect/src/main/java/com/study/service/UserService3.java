package com.study.service;

import com.study.entity.User;
import org.springframework.stereotype.Service;

/**
 * 测试继承/实现了 UserDao 的类下所有方法是否触发通知 ---- @Pointcut("this(com.study.service.UserDao)")
 *
 * @author Huang
 * @date 2019/3/26 22:16
 **/
@Service
public class UserService3 implements UserDao {

    @Override
    public User getById() {
        return null;
    }

    @Override
    public int deleteById(Long id) {
        return 0;
    }
}
