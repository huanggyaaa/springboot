package com.study.service;

import org.springframework.stereotype.Service;

/**
 * 测试这个类下所有方法是否触发通知 --- @Pointcut("within(com.study.service.UserService1)")
 *
 * @author Guangyue Huang
 * @date 2019/3/26 22:03
 **/
@Service
public class UserService1 {

    public void test() {
        System.out.println(1111);
    }

    public void test2() {
        System.out.println(2222);
    }
}
