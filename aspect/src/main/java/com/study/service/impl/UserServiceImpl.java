package com.study.service.impl;

import org.springframework.stereotype.Service;

/**
 * 测试 impl 包及其子包下面的方法是否触发通知 ---  @Pointcut("within(com.study.service.impl..*)")
 *
 * @author Guangyue Huang
 * @date 2019/3/26 22:22
 **/
@Service
public class UserServiceImpl {

    public void test() {
        System.out.println(111);
    }

}
