###### 启动类

+ 使用注解 @MapperScan 扫描 Mapper 接口
    * basePackages = "com.study.mapper" // Mybatis Mapper 接口路径
    * annotationClass = Repository.class // Mybatis Mapper 接口需要使用 @Repository 注解

```java
@SpringBootApplication
@MapperScan(basePackages = "com.study.mapper", annotationClass = Repository.class)
public class MybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisApplication.class, args);
    }

}
```

###### 配置文件

```properties
# 日期格式化
spring.jackson.date-format=yyyy-MM-dd HH:mm:ss
spring.jackson.time-zone=GMT+8

# 驱动会自动识别, 不需要显示指定
# spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://172.16.22.33:3306/test?useUnicode=true&characterEncoding=utf8
spring.datasource.username =root
spring.datasource.password =123456

# mybatis xml
mybatis.mapperLocations=classpath:mapper/*.xml

# mybatis sql, 两种方式都可以
# logging.level.com.study.mapper=debug
mybatis.configuration.log-impl=org.apache.ibatis.logging.stdout.StdOutImpl

# 实体包
mybatis.type-aliases-package=com.study.entity

# type-handler
mybatis.type-handlers-package=com.study.typehandle

# 驼峰转换
mybatis.configuration.map-underscore-to-camel-case=true

# 使用 DruidDataSource 数据源
# spring.datasource.type =com.alibaba.druid.pool.DruidDataSource
```

###### 数据实体

Tel 字段类型会做加密处理, 这个字段表示电话号码, 如果不需要做特殊处理使用 String 即可

```java
@Data
public class User implements Serializable {
    private Integer id;

    private Integer gender;

    private String username;

    private Date born;

    private Tel tel;

    private static final long serialVersionUID = 1L;

}
```

###### Mapper 接口

可以通过 xml 和 注解两种方式方式, ***如果使用注解 TypeHandler 不会生效***

```java
@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     * 注解
     */
    @Insert("INSERT INTO t_user(gender, username, born, tel) VALUES(#{gender}, #{username}, #{born}, #{tel})")
    int save(User user);

    @Delete("DELETE FROM t_user WHERE id = #{id}")
    int removeById(Integer id);

    @Update("UPDATE t_user SET gender = #{gender}, username = #{username}, born = #{born}, tel = #{tel.tel} WHERE id = #{id}")
    int updateById(User user);

    @Select("SELECT * FROM t_user WHERE id = #{id} and username = #{username}")
    User findByIdAndUsername(@Param("id") Integer id, @Param("username") String username);

    @Select("SELECT * FROM t_user")
    List<User> findAll();

    @Select("SELECT COUNT(1) FROM user")
    int count();

    /**
     * 返回 Map
     */
    @Select("SELECT * FROM t_user WHERE id = #{id}")
    Map<String, Object> getMapById(Integer id);

    @Select("SELECT * FROM t_user")
    List<Map<String, Object>> getAllMap();

    /**
     * 传入 Map
     */
    @Insert("INSERT INTO t_user(gender, username, born, tel) VALUES(#{gender}, #{username}, #{born}, #{tel})")
    int saveByMap(Map<String, Object> user);

}
```

###### TypeHandler

+ 实体中需要处理的字段需要自定义类, 把 java 数据实体的字段进行翻译/处理然后插入数据库

```java
@Alias("tel") // 指定别名
@Data
public class Tel {
    private String tel;
}
```

+ 创建字段翻译类并实现 BaseTypeHandler 接口和其方法
    * @MappedJdbcTypes({JdbcType.VARCHAR}) // 数据库使用 VARCHAR 类型
    * @MappedTypes({Tel.class}) // java 使用 Tel 类型
    * 泛型是 Tel 类

```java
/**
 * Tel 类型翻译, 只能通过 xml 方式, 注解或 Map 不生效
 *      MappedJdbcTypes: 数据库类型
 *      MappedTypes: java 类型, 可以是自定义/枚举等类型
 *
 * @author Huang
 * @date 2019/6/17 15:49
 */
@MappedJdbcTypes({JdbcType.VARCHAR})
@MappedTypes({Tel.class})
public class TelTypeHandler extends BaseTypeHandler<Tel> {

    /**
     * 入库
     *
     * @param preparedStatement 预编译 sql
     * @param i 参数位置
     * @param s 参数
     * @param jdbcType  jdbc类型
     */
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, Tel s, JdbcType jdbcType) throws SQLException {
        // 加密
        String str = PasswordUtil.base64Encry(s.getTel());
        // 设置到预编译 sql
        preparedStatement.setString(i, str);
    }

    /**
     * 出库, 从 ResultSet 中根据字段名称获取结果并转换
     *
     * @param resultSet 结果集
     * @param s 字段名称
     * @return 处理过后的 java 类型数据
     */
    @Override
    public Tel getNullableResult(ResultSet resultSet, String s) throws SQLException {
        // 数据库数据
        String tel = resultSet.getString(s);
        Tel tel1 = new Tel();
        tel1.setTel(PasswordUtil.base64Decry(tel));
        return tel1;
    }

    /**
     * 从 ResultSet 中根据字段顺序获取结果并转换
     *
     * @param resultSet 结果集
     * @param i 字段顺序
     * @return 处理过后的java类型数据
     */
    @Override
    public Tel getNullableResult(ResultSet resultSet, int i) throws SQLException {
        String tel = resultSet.getString(i);
        Tel tel1 = new Tel();
        tel1.setTel(PasswordUtil.base64Decry(tel));
        return tel1;
    }

    /**
     * 从 CallableStatement 中根据字段顺序获取结果并转换(存储过程)
     *
     * @param callableStatement 结果集
     * @param i 字段顺序
     * @return 处理过后的 java 类型数据
     */
    @Override
    public Tel getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        String tel = callableStatement.getString(i);
        Tel tel1 = new Tel();
        tel1.setTel(PasswordUtil.base64Decry(tel));
        return tel1;
    }
}
```

###### Mapper xml 文件

+ 在查询结果字段指定 typeHandler 属性, 查询出来的字段会自动翻译
+ 在修改和添加的 xml 代码块指定 typeHandler 属性表示插入和修改也会做字段翻译

```xml
<!-- 查询模块 -->
<resultMap id="BaseResultMap" type="com.study.entity.User">
    <id column="id" jdbcType="INTEGER" property="id" />
    <result column="gender" jdbcType="INTEGER" property="gender" />
    <result column="username" jdbcType="VARCHAR" property="username" />
    <result column="born" jdbcType="TIMESTAMP" property="born" />
    <result column="tel" jdbcType="VARCHAR" property="tel" typeHandler="com.study.config.TelTypeHandler"/>
  </resultMap>
<!-- 修改和插入类似这样 -->
<update id="updateByPrimaryKey" parameterType="com.study.entity.User">
    update t_user
    set gender = #{gender,jdbcType=INTEGER},
      username = #{username,jdbcType=VARCHAR},
      born = #{born,jdbcType=TIMESTAMP},
      tel = #{tel,jdbcType=VARCHAR, typeHandler=com.study.typehandle.TelTypeHandler}
    where id = #{id,jdbcType=INTEGER}
</update>
```


