package com.study.config;

import com.study.util.PasswordUtil;
import org.apache.ibatis.type.*;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Tel 类型翻译, 只能通过 xml 方式, 注解或 Map 不生效
 * MappedJdbcTypes: 数据库类型
 * MappedTypes: java 类型, 可以是自定义/枚举等类型
 *
 * @author Huang
 * @date 2019/6/17 15:49
 */
@MappedJdbcTypes({JdbcType.VARCHAR})
@MappedTypes({Tel.class})
public class TelTypeHandler extends BaseTypeHandler<Tel> {

    /**
     * 入库
     *
     * @param preparedStatement 预编译 sql
     * @param i                 参数位置
     * @param s                 参数
     * @param jdbcType          jdbc类型
     */
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, Tel s, JdbcType jdbcType) throws SQLException {
        // 加密
        String str = PasswordUtil.base64Encry(s.getTel());
        // 设置到预编译 sql
        preparedStatement.setString(i, str);
    }

    /**
     * 出库, 从 ResultSet 中根据字段名称获取结果并转换
     *
     * @param resultSet 结果集
     * @param s         字段名称
     * @return 处理过后的 java 类型数据
     */
    @Override
    public Tel getNullableResult(ResultSet resultSet, String s) throws SQLException {
        // 数据库数据
        String tel = resultSet.getString(s);
        Tel tel1 = new Tel();
        tel1.setTel(PasswordUtil.base64Decry(tel));
        return tel1;
    }

    /**
     * 从 ResultSet 中根据字段顺序获取结果并转换
     *
     * @param resultSet 结果集
     * @param i         字段顺序
     * @return 处理过后的java类型数据
     */
    @Override
    public Tel getNullableResult(ResultSet resultSet, int i) throws SQLException {
        String tel = resultSet.getString(i);
        Tel tel1 = new Tel();
        tel1.setTel(PasswordUtil.base64Decry(tel));
        return tel1;
    }

    /**
     * 从 CallableStatement 中根据字段顺序获取结果并转换(存储过程)
     *
     * @param callableStatement 结果集
     * @param i                 字段顺序
     * @return 处理过后的 java 类型数据
     */
    @Override
    public Tel getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        String tel = callableStatement.getString(i);
        Tel tel1 = new Tel();
        tel1.setTel(PasswordUtil.base64Decry(tel));
        return tel1;
    }
}
