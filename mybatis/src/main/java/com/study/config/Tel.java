package com.study.config;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Alias("tel") // 指定别名
@Data
public class Tel {
    private String tel;
}
