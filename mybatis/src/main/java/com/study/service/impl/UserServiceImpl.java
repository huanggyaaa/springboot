package com.study.service.impl;

import com.study.entity.User;
import com.study.mapper.UserMapper;
import com.study.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return userMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(User record) {
        return userMapper.insert(record);
    }

    @Override
    public int insertSelective(User record) {
        return userMapper.insertSelective(record);
    }

    @Override
    public User selectByPrimaryKey(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(User record) {
        return userMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(User record) {
        return userMapper.updateByPrimaryKey(record);
    }

    @Override
    public int save(User user) {
        return userMapper.save(user);
    }

    @Override
    public int removeById(Integer id) {
        return userMapper.removeById(id);
    }

    @Override
    public int updateById(User user) {
        return userMapper.updateById(user);
    }

    @Override
    public User findByIdAndUsername(Integer id, String username) {
        return userMapper.findByIdAndUsername(id, username);
    }

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public int count() {
        return userMapper.count();
    }

    @Override
    public Map<String, Object> getMapById(Integer id) {
        return userMapper.getMapById(id);
    }

    @Override
    public List<Map<String, Object>> getAllMap() {
        return userMapper.getAllMap();
    }

    @Override
    public int saveByMap(Map<String, Object> user) {
        return userMapper.saveByMap(user);
    }
}
