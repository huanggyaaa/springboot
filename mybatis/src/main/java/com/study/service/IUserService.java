package com.study.service;

import com.study.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface IUserService {

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     * 注解
     */
    @Insert("INSERT INTO t_user(gender, username, born, tel) VALUES(#{gender}, #{username}, #{born}, #{tel})")
    int save(User user);

    @Delete("DELETE FROM t_user WHERE id = #{id}")
    int removeById(Integer id);

    @Update("UPDATE t_user SET gender = #{gender}, username = #{username}, born = #{born}, tel = {tel.tel} WHERE id = #{id}")
    int updateById(User user);

    @Select("SELECT * FROM t_user WHERE id = #{id} and username = #{username}")
    User findByIdAndUsername(@Param("id") Integer id, @Param("username") String username);

    @Select("SELECT * FROM t_user")
    List<User> findAll();

    @Select("SELECT COUNT(1) FROM t_user")
    int count();

    /**
     * 返回 Map
     */
    @Select("SELECT * FROM t_user WHERE id = #{id}")
    Map<String, Object> getMapById(Integer id);

    @Select("SELECT * FROM t_user")
    List<Map<String, Object>> getAllMap();

    /**
     * 传入 Map
     */
    @Insert("INSERT INTO t_user(gender, username, born, tel) VALUES(#{gender}, #{username}, #{born}, #{tel})")
    int saveByMap(Map<String, Object> user);

}
