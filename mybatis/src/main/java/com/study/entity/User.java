package com.study.entity;

import com.study.config.Tel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * t_user
 *
 * @author huang
 */
@Data
public class User implements Serializable {
    private Integer id;

    private Integer gender;

    private String username;

    private Date born;

    private Tel tel;

    private static final long serialVersionUID = 1L;

}