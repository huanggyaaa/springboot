package com.study.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * 加密算法
 * 单向加密: 加密之后不能解密(MD5, SHA 家族, HMAC)
 * 对称加密: 可以加密, 也可以解密(Base64, DES, AES)
 * 非对称加密: 通过对应的密钥进行加解密(RSA, DSA, ECC)
 * 需要两个密钥：公开密钥(publickey)和私有密钥(privatekey)公开密钥与私有密钥是一对
 * 如果用公开密钥对数据进行加密，只有用对应的私有密钥才能解密, 如果用私有密钥对数据进行加密，那么只有用对应的公开密钥才能解密
 *
 * @author huang
 */
public class PasswordUtil {

    /**
     * base64 加密
     */
    public static String base64Encry(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * base64 解密
     */
    public static String base64Decry(String str) {
        byte[] decode = Base64.getDecoder().decode(str);
        return new String(decode, StandardCharsets.UTF_8);
    }

    /**
     * md5 加密
     */
    public static String md5(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] btInput = str.getBytes();
            md.update(btInput);
            byte[] btResult = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : btResult) {
                int bt = b & 0xff;
                if (bt < 16) {
                    sb.append(0);
                }
                sb.append(Integer.toHexString(bt));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * SHA-256 加密
     */
    public static String sha256(String str) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(str.getBytes(StandardCharsets.UTF_8));
            return byte2Hex(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 　　* 将 byte 转为 16 进制
     *
     */
    private static String byte2Hex(byte[] bytes) {
        StringBuilder stringBuffer = new StringBuilder();
        String temp;
        for (byte aByte : bytes) {
            temp = Integer.toHexString(aByte & 0xFF);
            if (temp.length() == 1) {
                //1得到一位的进行补0操作
                stringBuffer.append("0");
            }
            stringBuffer.append(temp);
        }
        return stringBuffer.toString();
    }

}
