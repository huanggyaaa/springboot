package com.study.service.impl;

import com.study.entity.User;
import com.study.service.IUserService;
import com.study.config.Tel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Resource
    private IUserService userService;

    @Test
    public void deleteByPrimaryKey() {
        userService.deleteByPrimaryKey(1);
    }

    @Test
    public void insert() {
        User user = new User();
        user.setBorn(new Date());
        user.setUsername("Bob");
        user.setGender(1);
        Tel tel = new Tel();
        tel.setTel("13934561234");
        user.setTel(tel);
        userService.insert(user);
    }

    @Test
    public void insertSelective() {
        User user = new User();
        user.setBorn(new Date());
        user.setUsername("Bob2");
        user.setGender(1);
        Tel tel = new Tel();
        tel.setTel("13023112212");
        user.setTel(tel);
        userService.insert(user);
    }

    @Test
    public void selectByPrimaryKey() {
        userService.selectByPrimaryKey(18);
    }

    @Test
    public void updateByPrimaryKeySelective() {
        User user = new User();
        user.setBorn(new Date());
        user.setUsername("Rose");
        user.setGender(0);
        user.setId(15);
        Tel tel = new Tel();
        tel.setTel("13723123212");
        user.setTel(tel);
        userService.updateByPrimaryKeySelective(user);
    }

    @Test
    public void updateByPrimaryKey() {
        User user = new User();
        user.setId(15);
        Tel tel = new Tel();
        tel.setTel("13423123212");
        user.setTel(tel);
        userService.updateByPrimaryKey(user);
    }

    @Test
    public void save() {
        User user = new User();
        user.setBorn(new Date());
        user.setUsername("Milk");
        user.setGender(1);
        Tel tel = new Tel();
        tel.setTel("13555555555");
        user.setTel(tel);
        userService.save(user);
    }

    @Test
    public void removeById() {
        userService.removeById(17);
    }

    @Test
    public void updateById() {
        User user = new User();
        user.setBorn(new Date());
        user.setUsername("July");
        user.setGender(1);
        Tel tel = new Tel();
        tel.setTel("13666666666");
        user.setTel(tel);
        user.setId(15);
        userService.updateById(user);
    }

    @Test
    public void findByIdAndUsername() {
        userService.findByIdAndUsername(18, "Bob");
    }

    @Test
    public void findAll() {
        userService.findAll();
    }

    @Test
    public void count() {
        userService.count();
    }

    @Test
    public void getMapById() {
        userService.getMapById(15);
    }

    @Test
    public void getAllMap() {
        userService.getAllMap();
    }

    @Test
    public void saveByMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("gender", 1);
        map.put("username", "Jim");
        map.put("born", new Date());
        map.put("tel", "18734561234");
        userService.saveByMap(map);
    }
}