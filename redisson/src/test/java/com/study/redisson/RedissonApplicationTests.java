package com.study.redisson;

import com.study.redisson.util.RedissonUtil;
import org.junit.jupiter.api.Test;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class RedissonApplicationTests {

    @Resource
    private RedissonUtil redissonUtil;

    private String lockKey = "redisLock_test";

    /**
     * 创建锁, 60 秒后自动释放, 自己加的锁必须自己解开, 不然就只能能过期自动解开或者成为死锁
     */
    @Test
    void getTryLockTest() {
        RLock lock = redissonUtil.getLock(lockKey);
        redissonUtil.tryLock(lock, 60);

        // 业务操作完成后释放锁
        try {
            Thread.sleep(3000);
            redissonUtil.unLock(lock);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放锁, 业务操作完前会失败
     */
    @Test
    void unLockTest() {
        RLock lock = redissonUtil.getLock(lockKey);
        redissonUtil.unLock(lock);
    }

    /**
     * 加锁, 业务操作完前会失败
     */
    void tryLockTest() {
        RLock lock = redissonUtil.getLock(lockKey);
        redissonUtil.tryLock(lock, 5);
    }

}
