###### 配置文件

简单设置 redis 地址即可, 本案例单纯测试 redisson

```properties
# redis
spring.redis.host=172.16.22.33
spring.redis.port=6379
```

###### 配置类

```java
@Configuration
public class RedissonConfig {
    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private String port;

    /*@Value("${spring.redis.password}")
    private String password;*/

    @Bean
    public RedissonClient getRedisson(){

        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + host + ":" + port);
        // 带密码
        // config.useSingleServer().setAddress("redis://" + host + ":" + port).setPassword(password);

        // 添加主从配置
        // config.useMasterSlaveServers().setMasterAddress("").setPassword("").addSlaveAddress(new String[]{"",""});

        return Redisson.create(config);
    }

}
```

###### 工具类

```java
/**
 * 简单的包装下 RedissonClient, 仅仅为了记录日志
 */
@Component
public class RedissonUtil {

    @Resource
    private RedissonClient redissonClient;

    /**
     * 创建锁
     *
     * @param key 锁的关键字（在redis中必须唯一）
     */
    public RLock getLock(String key){
        return redissonClient.getLock(key);
    }

    /**
     * 加锁
     *
     * @param rLock 锁对象
     * @param lockTime 锁的自动失效时间, 单位: 秒
     */
    public boolean tryLock(RLock rLock, long lockTime){
        boolean reBoolean = false;
        try {
            reBoolean = rLock.tryLock(lockTime, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.out.println("加锁失败，锁：" + rLock.getName());
        }
        return reBoolean;
    }

    /**
     * 释放锁
     */
    public void unLock(RLock rLock){
        if(null == rLock){
            return;
        }
        String name = rLock.getName();
        try {
            rLock.unlock();
            System.out.println("释放锁成功，锁：" + name);
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("释放锁失败，锁：" + (StringUtils.isEmpty(name) ? "" : name));
        }
    }
}
```

+ 测试

```java
@SpringBootTest
class RedissonApplicationTests {

    @Resource
    private RedissonUtil redissonUtil;

    private String lockKey = "redisLock_test";

    /**
     * 创建锁, 60 秒后自动释放, 自己加的锁必须自己解开, 不然就只能能过期自动解开或者成为死锁
     */
    @Test
    void getTryLockTest() {
        RLock lock = redissonUtil.getLock(lockKey);
        redissonUtil.tryLock(lock, 60);

        // 业务操作完成后释放锁
        try {
            Thread.sleep(3000);
            redissonUtil.unLock(lock);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放锁, 业务操作完前会失败
     */
    @Test
    void unLockTest(){
        RLock lock = redissonUtil.getLock(lockKey);
        redissonUtil.unLock(lock);
    }

    /**
     * 加锁, 业务操作完前会失败
     */
    void tryLockTest(){
        RLock lock = redissonUtil.getLock(lockKey);
        redissonUtil.tryLock(lock, 5);
    }

}
```
