package com.study.threadpool.service.impl;

import com.study.threadpool.service.IAsyncService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncServiceImpl implements IAsyncService {

    /**
     * 使用注解 @Async 把这个方法作为任务提交到线程池
     */
    @Override
    @Async
    public void hello() {
        System.out.println("hello");
    }
}
