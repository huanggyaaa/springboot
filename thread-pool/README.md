###### 创建配置类

```java
/**
 * @author huang
 */
@Configuration
@EnableAsync
public class ThreadPoolConf implements AsyncConfigurer {
    @Override
    public Executor getAsyncExecutor(){
        // 定义线程池
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 核心线程数
        executor.setCorePoolSize(10);
        // 最大线程数
        executor.setMaxPoolSize(100);
        // 队列容量
        executor.setQueueCapacity(1000);
        // 拒绝策略, 直接拒绝不抛出异常
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
        // 线程空闲存活时间(大于核心线程数的线程)
        executor.setKeepAliveSeconds(60);
        // 关闭线程时, 等待正在执行的任务执行完毕
        executor.setWaitForTasksToCompleteOnShutdown(true);
        return executor;
    }
}
```

###### 使用

方法上面加 @Async 注解即可异步提交任务

```java
@Service
public class AsyncServiceImpl implements IAsyncService {

    /**
     * 使用注解 @Async 把这个方法作为任务提交到线程池
     */
    @Override
    @Async
    public void hello() {
        System.out.println("hello");
    }
}
```
