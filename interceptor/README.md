###### 拦截器操作类

+ 创建类实现 HandlerInterceptor 接口和其方法, 使用注解 @Component 交由 spring 管理

```java
@Component
public class LoginInterceptor implements HandlerInterceptor {

    /**
     * 过滤器不能注释 bean
     */
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 进入接口方法前触发
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        Object key = stringRedisTemplate.opsForValue().get("key");

        // 请求头参数
        String token1 = request.getHeader("token");

        // 获取请求参数, 进行验证( filter 只能跳转页面, 拦截器可以返回类容 )
        String token = request.getParameter("token");

        // 放行
        if (StringUtils.isEmpty(token)){
            response.getWriter().print("token err");
            return false;
        }
        // 拒绝请求
        return true;
    }

    /**
     * 接口调用之后, 试图渲染之前触发
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    /**
     * 试图渲染完成之后触发, 标志着接口调用流程结束, 通常用于资源清理
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

}
```

###### 拦截器配置类

+ 创建类实现 WebMvcConfigurer 接口和其方法
+ 拦截规则:
    * /api/login/**: 以 /api/login/ 打头的接口, 多级也会拦截
    * /api/login/*: 和上面的区别是只拦截一级, 比如 /api/login/a/b 就不会被拦截
+ addInterceptors 方法中组合拦截器操作类和请求(每个拦截器操作类处理不同的请求)
+ 代码示例:

```java
@Component
public class InterceptorConfig implements WebMvcConfigurer {

    /**
     * 注入拦截器 loginInterceptor
     */
    @Resource
    private LoginInterceptor loginInterceptor;
    /**
     * 注入拦截器 productInterceptor
     */
    @Resource
    private ProductInterceptor productInterceptor;

    /**
     * 登录相关接口
     */
    private final String LOGIN_URL = "/api/login/**";

    /**
     * 商品相关接口
     */
    private final String PRODUCT_URL = "/api/product/**";

    /**
     * 访客 商品相关接口
     */
    private final String GUEST_PRODUCT_URL = "/api/product/guest/**";

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        /**
         * 登录接口拦截配置
         */
        registry.addInterceptor(loginInterceptor).addPathPatterns(LOGIN_URL);

        /**
         * 商品接口拦截配置
         *  excludePathPatterns 在 addPathPatterns 的基础上, 指定的接口不拦截
         */
        registry.addInterceptor(productInterceptor).addPathPatterns(PRODUCT_URL).excludePathPatterns(GUEST_PRODUCT_URL);

        WebMvcConfigurer.super.addInterceptors(registry);
    }

}
```

