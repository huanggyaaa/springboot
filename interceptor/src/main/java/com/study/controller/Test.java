package com.study.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Test {

    /**
     * 会被拦截, 验证是否有 token
     */
    @GetMapping("/api/login/{username}")
    public String test1(@PathVariable String username) {
        return username;
    }

    /**
     * 不会被拦截
     */
    @GetMapping("/api/test")
    public String test2() {
        return "test2";
    }

    /**
     * 不会被拦截
     */
    @GetMapping("/api/v1/login")
    public String test3() {
        return "test3";
    }

    /**
     * 会被拦截, 验证是否有 token
     */
    @GetMapping("/api/product/{id}")
    public String test4(@PathVariable String id) {
        return id;
    }

    /**
     * 不会被拦截
     */
    @GetMapping("/api/product/guest/{id}")
    public String test5(@PathVariable String id) {
        return id;
    }
}
