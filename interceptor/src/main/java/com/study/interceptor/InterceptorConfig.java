package com.study.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * 拦截器, url 拦截配置如下:
 *
 * @author huang
 */
@Component
public class InterceptorConfig implements WebMvcConfigurer {

    /**
     * 注入拦截器 loginInterceptor
     */
    @Resource
    private LoginInterceptor loginInterceptor;
    /**
     * 注入拦截器 productInterceptor
     */
    @Resource
    private ProductInterceptor productInterceptor;

    /**
     * 登录相关接口
     */
    private final String LOGIN_URL = "/api/login/**";

    /**
     * 商品相关接口
     */
    private final String PRODUCT_URL = "/api/product/**";

    /**
     * 访客 商品相关接口
     */
    private final String GUEST_PRODUCT_URL = "/api/product/guest/**";

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        /**
         * 登录接口拦截配置
         */
        registry.addInterceptor(loginInterceptor).addPathPatterns(LOGIN_URL);

        /**
         * 商品接口拦截配置
         *  excludePathPatterns 在 addPathPatterns 的基础上, 指定的接口不拦截
         */
        registry.addInterceptor(productInterceptor).addPathPatterns(PRODUCT_URL).excludePathPatterns(GUEST_PRODUCT_URL);

        WebMvcConfigurer.super.addInterceptors(registry);
    }

}
