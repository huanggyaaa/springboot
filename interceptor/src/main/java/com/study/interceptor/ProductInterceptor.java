package com.study.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 商品拦截器
 *
 * @author huang
 */
@Component
public class ProductInterceptor implements HandlerInterceptor {

    /**
     * 指定的接口进行拦截
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 获取请求参数, 进行验证( filter 只能跳转页面, 拦截器可以返回类容 )
        String token = request.getParameter("token");

        // 放行
        if (StringUtils.isEmpty(token)) {
            response.getWriter().print("token err");
            return false;
        }
        // 拒绝请求
        return true;
    }

}
