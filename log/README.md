###### 配置文件

指定日志框架类型

```properties
logging.config=classpath:logback-spring.xml
```

###### 具体配置

日志配置文件安排的明明白白的, [跳转到日志配置文件](./src/main/resources/logback-spring.xml)
