package com.study.jwt.controller;

import com.study.jwt.util.JwtUtil;
import io.jsonwebtoken.Claims;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author huanggy
 * @date 2020/2/8 15:54
 */
@RestController
public class Controller {

    @GetMapping("/test/{username}/{password}")
    public String login(@PathVariable String username, @PathVariable String password) {

        return JwtUtil.createJwt(UUID.randomUUID().toString(), "huanggy", username.concat(password));
    }

    @GetMapping("/test/{token}")
    public Object parseToken(@PathVariable String token) {
        Claims claims = JwtUtil.parseJwt(token);
        if (claims == null) return "token 不合法";
        return claims;
    }
}
