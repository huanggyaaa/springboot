package com.study.jwt.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.*;

/**
 * JJWT 工具包
 *
 * @author huanggy
 * @date 2020/2/8 11:27
 */
public class JwtUtil {

    private static final int TTL = 1000 * 20;
    private static final String SECRET = "secret";

    /**
     * 生成 token
     *
     * @param id      唯一 ID
     * @param issuer  签发人
     * @param subject 用户信息
     */
    public static String createJwt(String id, String issuer, String subject) {

        // 算法
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        // 生成时间
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        // 过期时间
        Date expire = new Date(nowMillis + TTL);

        // Payload 私有声明, 非必须, 让 token 信息更丰富
        Map<String, Object> claims = new HashMap<>(3);
        claims.put("uid", "123456");
        claims.put("user_name", "admin");
        claims.put("nick_name", "X-rapido");

        // 设置 Payload
        JwtBuilder builder = Jwts.builder()
                .setClaims(claims)          // 如果有私有声明必须写在前面
                .setId(id)                  // jwt ID 唯一
                .setIssuedAt(now)           // 签发时间
                .setIssuer(issuer)          // 签发人
                .setExpiration(expire)      // 过期时间
                .setSubject(subject)        // 用户信息, 可以是用户 ID 或用户信息 json 串
                .signWith(signatureAlgorithm, SECRET); // 算法和密匙

        // 返回 token
        return builder.compact();
    }

    /**
     * 解析 token
     *
     * @param jwt 要解密的 jwt
     */
    public static Claims parseJwt(String jwt) {

        // 执行解密
        try {
            return Jwts.parser()    // 得到DefaultJwtParser
                    .setSigningKey(SECRET)                 //设置签名的秘钥
                    .parseClaimsJws(jwt).getBody();

        } catch (Exception e) {
            // 解析失败, token 不合法
            return null;
        }
    }


}
