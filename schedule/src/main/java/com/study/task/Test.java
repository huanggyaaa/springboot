package com.study.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 项目启动, 就会执行所有任务
 * 异步任务可以手写线程来实现
 */
@Component
public class Test {

    /**
     * 上次任务完成后再执行，参数类型为long，单位ms
     */
    @Scheduled(fixedDelay = 1000)
    public void currentTime() {
        System.out.println("当前时间: " + new Date());
    }

    /**
     * 定期执行, 不管上次任务是否完成
     */
    @Scheduled(fixedRate = 1000)
    public void test2() {
        System.out.println("it is time");
    }

    /**
     * cron 表达式，指定任务在特定时间执行
     * "0 0 * * * *"           表示每小时0分0秒执行一次, 整点执行
     * "0 0 8-10 * * *"        表示每天8，9，10点执行
     * "0 0/30 8-10 * * *"     表示每天8点到10点，每半小时执行
     * "0 0 9-17 * * MON-FRI"  表示每周一至周五，9点到17点的0分0秒执行
     * "0 0 0 25 12 ?"         表示每年圣诞节（12月25日）0时0分0秒执行
     */
    @Scheduled(cron = "0 0 0 25 12 ?")
    public void test1() {
        System.out.println("圣诞节到了...");
    }

}
