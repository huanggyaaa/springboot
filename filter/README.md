###### 定义过滤器

+ 缺点: ***不能注入 bean, 比如不能注入 RedisTemplate***
+ 实现 Filter 接口, 覆写方法
+ 使用 @WebFilter 标注这是一个过滤器
    * urlPatterns = "/api/*"    // 配置拦截规则, /api 打头的请求会被过滤
    * filterName = "loginFilter"    // 定义拦截器名称
+ 代码示例

```java
/**
 * LoginFilter 过滤器配置
 * @author huang
 */
@WebFilter(urlPatterns = "/api/*", filterName = "loginFilter")
public class LoginFilter implements Filter {

    /**
     * 项目启动触发
     */
    @Override
    public void init(FilterConfig filterConfig) {
        System.out.println("过滤器初始化完成...");
    }

    /**
     * 请求被拦截时触发
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        // 获取请求附带参数, 进行逻辑验证, 决定是否放行
        req.getParameter("username");
        req.getParameter("password");
        String token = req.getParameter("token");

        if (!StringUtils.isEmpty(token)){
            // 放行
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            // 拒绝; 没有返回值, 所以页面不能通过响应做进一步处理, 过滤器适用于非前后分离的项目
            System.out.println("token 不正确");
            // 跳转页面到 index.html
            resp.sendRedirect("/index.html");
        }
    }

    /**
     * 项目停止触发
     */
    @Override
    public void destroy() {
        System.out.println("com.study.filter.LoginFilter destroy...");
    }
}
```

###### 控制层

```java
@RestController
public class Test {

    /**
     * 这个请求会被拦截, 判断是否有 token
     */
    @GetMapping("/api/test1/{username}")
    public String test1 (@PathVariable String username){
        return "test1 ====>  username:" + username;
    }

    /**
     * 这个请求也会被拦截, 判断是否有 token
     */
    @GetMapping("/api/test2")
    public String test2 (){
        return "test2";
    }

    /**
     * 这个请求不会被拦截
     */
    @GetMapping("/test3")
    public String test3 (){
        return "test3";
    }

}
```
