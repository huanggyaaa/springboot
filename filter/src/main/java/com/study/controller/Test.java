package com.study.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Test {

    /**
     * 这个请求会被拦截, 判断是否有 token
     */
    @GetMapping("/api/test1/{username}")
    public String test1(@PathVariable String username) {
        return "test1 ====>  username:" + username;
    }

    /**
     * 这个请求也会被拦截, 判断是否有 token
     */
    @GetMapping("/api/test2")
    public String test2() {
        return "test2";
    }

    /**
     * 这个请求不会被拦截
     */
    @GetMapping("/test3")
    public String test3() {
        return "test3";
    }

}
