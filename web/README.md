###### 资源访问

+ resources/static 可以直接访问, resources/templates 只能通过控制层访问

```java
@Controller
public class WebController {
    /**
     * 首页, 页面在 templates 下, 必须通过控制层访问, 页面引入了 static 的 js 和 css
     */
    @GetMapping("/index")
    public String pageIndex(){
        return "index";
    }

    /**
     * 新闻页
     */
    @GetMapping("/news")
    public String pageNews(){
        return "pages/news";
    }
}
```

###### 数据验证

+ 封装实体

```java
/**
 * @author huanggy
 * @date 2019/7/9 17:33
 */
@Data
public class UserDTO {

    @NotNull(message = "ID 不能为空")
    private Integer id;

    @Future(message = "日期只能是将来时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date date;

    @NotNull
    @DecimalMin(value = "0.1", message = "最小值为 0.1")
    @DecimalMax(value = "10000.00", message = "最大值为 10000.00")
    private Double doubleVal;

    @NotNull
    @Min(value = 1, message = "最小值为1")
    @Max(value = 100, message = "最大值为100")
    private Integer intVal;

    // 可以为空
    @Range(min = 1, max = 100, message = "合法取值范围: 1 - 100")
    private Integer range;

    @Email(message = "邮箱不合法")
    @NotNull
    private String email;

    @Size(min = 1, max = 30, message = "字数为1-30之间")
    private String description;

}
```

+ 验证工具类

```java
@Component
public class ValiUtil implements InitializingBean {

    private Validator validator;

    // 实现校验方法并返回校验结果
    public ValiResult validate(Object bean){
        ValiResult validationResult = new ValiResult();
        Set<ConstraintViolation<Object>> constraintViolationSet = validator.validate(bean);
        if(constraintViolationSet.size() > 0){
            // 大于0 表示有错误
            validationResult.setHasErrors(true);
            for (ConstraintViolation<Object> constraintViolation : constraintViolationSet) {
                String errorMsg = constraintViolation.getMessage();
                String propertyName = constraintViolation.getPropertyPath().toString();
                validationResult.getErrorMsgMap().put(propertyName, errorMsg);
            }
        }
        return validationResult;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // 将hibernate validator通过工厂的初始化方式使其实例化
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }
}
```

+ 验证结果类

```java
public class ValiResult {
    // 校验结果是否有错
    private boolean hasErrors = false;

    // 存放错误信息的Map
    private Map<String, String> errorMsgMap = new HashMap<>();

    public boolean isHasErrors() {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors) {
        this.hasErrors = hasErrors;
    }

    public Map<String, String> getErrorMsgMap() {
        return errorMsgMap;
    }

    public void setErrorMsgMap(Map<String, String> errorMsgMap) {
        this.errorMsgMap = errorMsgMap;
    }
}
```

+ 测试

```java
@Controller
public class WebController {
    @PostMapping("/post2")
    @ResponseBody
    public Object testPost2(@RequestBody UserDTO dto){
        ValiResult validate = valiUtil.validate(dto);
        if (validate.isHasErrors()){
            return validate;
        }
        return "success";
    }
}
```

###### 自带字段验证

```java
@Controller
public class WebController {
    /**
     * 如果有验证失败的字段, 将放入自动 errors 中
     */
    @PostMapping("/post1")
    @ResponseBody
    public Map<String, String> testPost(@Valid @RequestBody UserDTO userDTO, Errors errors){
        List<ObjectError> allErrors = errors.getAllErrors();
        if (allErrors != null && allErrors.size() > 0) {

            HashMap<String, String> map = new HashMap<>(16);

            allErrors.forEach((item) -> {
                if (item instanceof FieldError) {
                    FieldError item1 = (FieldError) item;
                    // 字段
                    String field = item1.getField();
                    // 提示信息
                    String defaultMessage = item.getDefaultMessage();
                    map.put(field, defaultMessage);
                }
            });

            return map;
        }
        return null;
    }
}
```

###### 请求参数接收

```java
@Controller
public class WebController {
    /**
     * 不使用注解也能接受参数, 只要前端请求参数和后台定义的一致即可, 只适用于 get 请求
     */
    @GetMapping("/get1")
    @ResponseBody
    public Map<String, Object> testGet1(Integer intVal, Long longVal, String str){
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("intVal", intVal);
        stringObjectHashMap.put("longVal", longVal);
        stringObjectHashMap.put("str", str);
        return stringObjectHashMap;
    }

    /**
     * 前端使用 int_val 后台使用 intVal 接受, required = false, 可以为 null
     * @RequestParam Long longVal 前端的参数必须是 longVal , 不能为 null
     */
    @GetMapping("/get2")
    @ResponseBody
    public Map<String, Object> testGet2(@RequestParam(value = "int_val", required = false) Integer intVal,
                                        @RequestParam Long longVal,
                                        @RequestParam String str){
        HashMap<String, Object> stringObjectHashMap = new HashMap<>(3);
        stringObjectHashMap.put("intVal", intVal);
        stringObjectHashMap.put("longVal", longVal);
        stringObjectHashMap.put("str", str);
        return stringObjectHashMap;
    }
}
```