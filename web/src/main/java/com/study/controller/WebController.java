package com.study.controller;

import com.study.dto.UserDTO;
import com.study.util.ValiResult;
import com.study.util.ValiUtil;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class WebController {

    @Resource
    private ValiUtil valiUtil;

    /**
     * 首页
     */
    @GetMapping("/index")
    public String pageIndex() {
        return "index";
    }

    /**
     * 新闻页
     */
    @GetMapping("/news")
    public String pageNews() {
        return "pages/news";
    }

    /**
     * 不使用注解也能接受参数, 只要前端请求参数和后台定义的一致即可, 只适用于 get 请求
     */
    @GetMapping("/get1")
    @ResponseBody
    public Map<String, Object> testGet1(Integer intVal, Long longVal, String str) {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("intVal", intVal);
        stringObjectHashMap.put("longVal", longVal);
        stringObjectHashMap.put("str", str);
        return stringObjectHashMap;
    }

    /**
     * 前端使用 int_val 后台使用 intVal 接受, required = false, 可以为 null
     *
     * @RequestParam Long longVal 前端的参数必须是 longVal , 不能为 null
     */
    @GetMapping("/get2")
    @ResponseBody
    public Map<String, Object> testGet2(@RequestParam(value = "int_val", required = false) Integer intVal,
                                        @RequestParam Long longVal,
                                        @RequestParam String str) {
        HashMap<String, Object> stringObjectHashMap = new HashMap<>(3);
        stringObjectHashMap.put("intVal", intVal);
        stringObjectHashMap.put("longVal", longVal);
        stringObjectHashMap.put("str", str);
        return stringObjectHashMap;
    }

    /**
     * 如果有验证失败的字段, 将放入自动 errors 中
     */
    @PostMapping("/post1")
    @ResponseBody
    public Map<String, String> testPost(@Valid @RequestBody UserDTO userDTO, Errors errors) {
        List<ObjectError> allErrors = errors.getAllErrors();
        if (allErrors != null && allErrors.size() > 0) {

            HashMap<String, String> map = new HashMap<>(16);

            allErrors.forEach((item) -> {
                if (item instanceof FieldError) {
                    FieldError item1 = (FieldError) item;
                    // 字段
                    String field = item1.getField();
                    // 提示信息
                    String defaultMessage = item.getDefaultMessage();
                    map.put(field, defaultMessage);
                }
            });

            return map;
        }
        return null;
    }

    /**
     * 字段验证, 封装后的验证, 简单说明哪个字段有什么问题, 冗余信息没有上面的 Errors 多
     */
    @PostMapping("/post2")
    @ResponseBody
    public Object testPost2(@RequestBody UserDTO dto) {
        ValiResult validate = valiUtil.validate(dto);
        if (validate.isHasErrors()) {
            return validate;
        }
        return "success";
    }

}
