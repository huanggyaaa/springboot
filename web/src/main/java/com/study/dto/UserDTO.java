package com.study.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * @author huanggy
 * @date 2019/7/9 17:33
 */
@Data
public class UserDTO {

    @NotNull(message = "ID 不能为空")
    private Integer id;

    @Future(message = "日期只能是将来时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date date;

    @NotNull
    @DecimalMin(value = "0.1", message = "最小值为 0.1")
    @DecimalMax(value = "10000.00", message = "最大值为 10000.00")
    private Double doubleVal;

    @NotNull
    @Min(value = 1, message = "最小值为1")
    @Max(value = 100, message = "最大值为100")
    private Integer intVal;

    // 可以为空
    @Range(min = 1, max = 100, message = "合法取值范围: 1 - 100")
    private Integer range;

    @Email(message = "邮箱不合法")
    @NotNull
    private String email;

    @Size(min = 1, max = 30, message = "字数为1-30之间")
    private String description;

}
