#### 排除自动配置的单数据源

首先要在启动类排除默认的单数据源配置类，因为它会读取 application.properties 文件的 spring.datasource.* 属性并自动配置单数据源

```java
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
```

#### 配置文件配置多数据源

```properties
# database
db.conn.str = useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&useLocalSessionState=true&tinyInt1isBit=false
# 数据源1
spring.datasource.db1.jdbc-url=jdbc:mysql://192.168.7.153:3306/db1test?${db.conn.str}
spring.datasource.db1.username=root
spring.datasource.db1.password=123456
spring.datasource.db1.driver-class-name=com.mysql.cj.jdbc.Driver
# 数据源2
spring.datasource.db2.jdbc-url=jdbc:mysql://192.168.7.158:3306/db2test?${db.conn.str}
spring.datasource.db2.username=root
spring.datasource.db2.password=123456
spring.datasource.db2.driver-class-name=com.mysql.cj.jdbc.Driver
```

#### 手动创建数据库配置类

```java
@Configuration
public class DataSourceConfig {

    @Bean(name = "db1")
    @ConfigurationProperties(prefix = "spring.datasource.db1")
    public DataSource db1() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "db2")
    @ConfigurationProperties(prefix = "spring.datasource.db2")
    public DataSource db2() {
        return DataSourceBuilder.create().build();
    }
}
```

#### 根据不同数据源配置 mybatis 的 SessionFactory

目的是让不同包下的 mapper 和 mybatis 的 xml 文件 自动使用不同的数据源

```java
/**
 * @author huanggy
 */
@Configuration
@MapperScan(basePackages = {"com.study.multipledatasources.mapper.db1"}, sqlSessionFactoryRef = "sqlSessionFactoryDb1", annotationClass = Repository.class)
public class Db1Config {

    @Autowired
    @Qualifier("db1")
    private DataSource dataSourceDb1;

    @Bean
    public SqlSessionFactory sqlSessionFactoryDb1() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSourceDb1);
        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/db1/*.xml"));
        return factoryBean.getObject();
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplateDb1() throws Exception {
        return new SqlSessionTemplate(sqlSessionFactoryDb1());
    }
}
/**
 * @author huanggy
 */
@Configuration
@MapperScan(basePackages = {"com.study.multipledatasources.mapper.db2"}, sqlSessionFactoryRef = "sqlSessionFactoryDb2", annotationClass = Repository.class)
public class Db2Config {

    @Autowired
    @Qualifier("db2")
    private DataSource dataSourceDb2;

    @Bean
    public SqlSessionFactory sqlSessionFactoryDb2() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSourceDb2);
        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/db2/*.xml"));
        return factoryBean.getObject();
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplateDb2() throws Exception {
        return new SqlSessionTemplate(sqlSessionFactoryDb2());
    }
}
```