package com.study.multipledatasources.controller;

import com.study.multipledatasources.entity.db2.Teacher;
import com.study.multipledatasources.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private ITeacherService teacherService;

    /**
     * 添加记录
     */
    @PostMapping("/")
    public Object insert(@RequestBody Teacher teacher) {
        return teacherService.insertSelective(teacher);
    }

}
