package com.study.multipledatasources.controller;

import com.study.multipledatasources.entity.db1.Student;
import com.study.multipledatasources.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private IStudentService studentService;

    /**
     * 添加记录
     */
    @PostMapping("/")
    public Object insert(@RequestBody Student student) {
        return studentService.insertSelective(student);
    }
}
