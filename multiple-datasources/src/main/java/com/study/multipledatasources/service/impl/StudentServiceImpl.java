package com.study.multipledatasources.service.impl;

import com.study.multipledatasources.entity.db1.Student;
import com.study.multipledatasources.mapper.db1.StudentMapper;
import com.study.multipledatasources.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements IStudentService {

    @Autowired
    private StudentMapper mapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Student record) {
        return mapper.insert(record);
    }

    @Override
    public int insertSelective(Student record) {
        return mapper.insertSelective(record);
    }

    @Override
    public Student selectByPrimaryKey(Integer id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Student record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Student record) {
        return mapper.updateByPrimaryKey(record);
    }
}
