package com.study.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * mongo 实体
 *
 * @author huang
 */
@Document(collection = "user_info")
@Data
public class UserInfo {

    private String phone;

    private String name;

    private Integer sex;

    private Integer age;
}
