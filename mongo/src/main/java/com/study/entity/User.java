package com.study.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;

/**
 * mongo 实体
 *
 * @author huang
 **/
@Data
@Document(collection = "user")
public class User {

    @Id
    private Long id;

    private String username;

    private String password;

    @Field("register_time")
    private Date registerTime;

    private List<UserInfo> infos;

}
