package com.study.repository;

import com.study.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 直接继承 MongoRepository 即可
 *
 * @author huang
 */
public interface UserRepositoryMongo extends MongoRepository<User, Long> {
}
