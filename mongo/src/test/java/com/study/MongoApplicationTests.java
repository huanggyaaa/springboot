package com.study;

import com.study.entity.User;
import com.study.entity.UserInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 原生操作
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MongoApplicationTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 插入数据
     */
    @Test
    public void contextLoads() {

        User user = new User();
        user.setId(10002L);
        user.setPassword("111111");
        user.setRegisterTime(new Date());
        user.setUsername("水晶之恋");
        ArrayList<UserInfo> userInfos = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            UserInfo userInfo = new UserInfo();
            userInfo.setAge(20);
            userInfo.setName("info-" + (i + 1));
            userInfo.setPhone("123123123");
            userInfo.setSex(0);
            userInfos.add(userInfo);
        }
        user.setInfos(userInfos);

        // 插入数据
        mongoTemplate.insert(user);

        // 有数据就更新，没有就插入
        mongoTemplate.save(user);

    }

    /**
     * 更新数据
     */
    @Test
    public void update() {

        Query query = new Query().addCriteria(Criteria.where("username").is("德玛西亚"));
        Update update = new Update().set("username", "往事随风").set("age", 25).set("name", "Milk").set("sex", 1);

        // 更新第一条
        mongoTemplate.updateFirst(query, update, User.class);

        // 批量修改
        mongoTemplate.updateMulti(query, update, User.class);

        // 更新第一条, 没有就添加
        mongoTemplate.upsert(query, update, User.class);

    }

    /**
     * 删除数据
     */
    @Test
    public void delete() {

        Query query = new Query().addCriteria(Criteria.where("_id").is(10003L));
        mongoTemplate.remove(query, "user");

        // 批量删除
        Query query1 = new Query().addCriteria(Criteria.where("username").is("往事随风"));
        mongoTemplate.remove(query1, "user");

        // 批量删除
        User user = new User();
        user.setId(10001L);
        mongoTemplate.remove(user);

    }

    /**
     * 查询
     */
    @Test
    public void find() {

        // 通过 ID 查询
        User user = mongoTemplate.findById(10001L, User.class);

        // 条件查询
        Query query = new Query().addCriteria(Criteria.where("username").is("水晶之恋").and("password").is("111111"));
        List<User> users = mongoTemplate.find(query, User.class);

        // 查询全部
        List<User> list = mongoTemplate.findAll(User.class);

        // 查询总数
        long count = mongoTemplate.count(query, User.class);

        // 是否存在
        boolean exists = mongoTemplate.exists(query, User.class);
    }

    /**
     * 复杂查询
     */
    @Test
    public void search() {

        // 匹配“水晶”开头
        Query query = new Query().addCriteria(Criteria.where("username").regex(Pattern.compile("^水晶.*$", Pattern.CASE_INSENSITIVE)));

        // 匹配“水晶”结尾
        Query query2 = new Query().addCriteria(Criteria.where("username").regex(Pattern.compile("^.*水晶$", Pattern.CASE_INSENSITIVE)));

        // 匹配包含“水晶”
        Query query3 = new Query().addCriteria(Criteria.where("username").regex(Pattern.compile("^.*水晶.*$", Pattern.CASE_INSENSITIVE)));

        // 范围查询, 大于小于
        Query query4 = new Query().addCriteria(Criteria.where("createTime").gte(new Date()).lte(new Date()));

        // 根据子文档查询
        Query query5 = new Query().addCriteria(Criteria.where("userInfo.phone").is("123123123"));

        // username 是 Marry 或者 Bob 或者 Rose
        Query query6 = new Query().addCriteria(Criteria.where("userInfo.name").in("Marry", "Bob", "Rose"));

        // username 不是 Marry 也不是 Bob 也不是 Rose
        Query query7 = new Query().addCriteria(Criteria.where("username").nin("Marry", "Bob", "Rose"));

        // 根据 ID 倒叙, 排序字段是一个可变参数
        Sort sort = new Sort(Sort.Direction.DESC, "_id");

        // 排序, 分页, 类似 mysql limit
        List<User> list = mongoTemplate.find(query.with(sort).skip(1).limit(2), User.class);

    }

}
