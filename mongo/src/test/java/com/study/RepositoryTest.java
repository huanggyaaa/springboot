package com.study;

import com.study.entity.User;
import com.study.repository.UserRepositoryMongo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Repository 操作
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTest {
    @Autowired
    private UserRepositoryMongo userRepository;
    List<User> list = new ArrayList<>();

    {
        for (int i = 2; i < 14; i++) {
            User user = new User();
            user.setId((long) i);
            user.setPassword("111111");
            user.setRegisterTime(new Date());
            user.setUsername("Bob");
            list.add(user);
        }
    }

    @Test
    public void test1() {
        User user = new User();
        user.setId(1L);
        user.setPassword("111111");
        user.setRegisterTime(new Date());
        user.setUsername("Rose");

        // 新增
        User save = userRepository.insert(user);

        // 批量新增
        List<User> insert = userRepository.insert(list);

        // ID 查询
        User user1 = userRepository.findById(1L).get();

        // 总数
        long count = userRepository.count();

        // 查询所有
        List<User> all = userRepository.findAll();

        // 排序, ID 降序
        Sort sort1 = new Sort(Sort.Direction.DESC, "id");
        List<User> all4 = userRepository.findAll(sort1);

        // ID 为2 的是否存在
        boolean b = userRepository.existsById(2L);

        // 分页查询, 第一页, 每页3条数据
        Pageable pageable = PageRequest.of(1, 3);
        Page<User> all3 = userRepository.findAll(pageable);

        // Example 查询
        User queryUser = new User();
        queryUser.setUsername("Mar");
        queryUser.setPassword("1111111");
        ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                .withMatcher("username", ExampleMatcher.GenericPropertyMatchers.startsWith()) // username Mar 开头
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains()); // name 包含 os
        Example<User> example = Example.of(queryUser, exampleMatcher);
        // 很多方法都可以使用 example
        List<User> all1 = userRepository.findAll(example);
        boolean exists = userRepository.exists(example);
        long count1 = userRepository.count(example);

        // 排序
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        List<User> all2 = userRepository.findAll(example, sort);

        // 修改
        user.setUsername("Milk");
        User save1 = userRepository.save(user);

        // 删除
        userRepository.deleteById(1L);

    }
}
