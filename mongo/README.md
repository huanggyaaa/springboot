###### docker 搭建 mongo 环境

+ docker pull mongo // 拉取镜像
+ docker run -p 27017:27017 -v $PWD/db:/data/db -d mongo:latest // 运行

###### 配置文件

```properties
# 单机
spring.data.mongodb.host=172.16.22.33
spring.data.mongodb.port=27017
spring.data.mongodb.database=test
```

###### 数据实体

+ 包含子文档所以有两个实体
+ @Document(collection = "user") 标注为 mongo 实体, collection 指定数据表名
    * @Field("register_time") 字段映射, 注解里面的值是 mongo 实际存储的字段名
    * @Id 主键, 如果不设置 mongo 会自动生成 ID

```java
// 实体 1
@Data
@Document(collection = "user")
public class User {

    @Id
    private Long id;

    private String username;

    private String password;

    @Field("register_time")
    private Date registerTime;

    private List<UserInfo> infos;

}

// 实体 2
@Document(collection = "user_info")
@Data
public class UserInfo {

    private String phone;

    private String name;

    private Integer sex;

    private Integer age;
}
```

###### repository

泛型为实体和主键类型

```java
/**
 * 直接继承 MongoRepository 即可
 *
 * @author huang
 */
public interface UserRepositoryMongo extends MongoRepository<User, Long> {
}
```

###### 查询

+ 通过 repository 查询
    * 详见 repository 测试类, [跳转测试类](./src/test/java/com/study/RepositoryTest.java)
+ 通过 MongoTemplate 查询, 这种查询方式的话不需要构建 repository 类
    * 详见 MongoTemplate 测试类, [跳转测试类](./src/test/java/com/study/MongoApplicationTests.java)

