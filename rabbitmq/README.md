#### docker 搭建 rabbitMQ 环境

+ docker pull rabbitmq:management // 拉取镜像
+ docker run -d --hostname my-test_host --name my_rabbit -p 15672:15672 -p 5672:5672 rabbitmq:management // 运行, 默认用户名:
  guest; 密码: guest
+ docker run -d --hostname test_host --name my_rabbit -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin -p
  15672:15672 -p 5672:5672 rabbitmq:management // 运行, 指定用户名(admin), 密码(admin)

#### 工作模式

##### simple

+ 消息生产者产生消息，并放入队列
+ 消费者监听队列，拿到消息之后进行消费，拿到消息之后从队列里面删除消息
+ 多个消费者监听同一个队列，消费者会轮询去消费消息

##### simple + ack

+ 消费者拿到消息之后，就从队列删除消息，有可能在消费的时候出现异常导致拿到消息但是消费不成功
+ ack 机制：拿到消息后，消费成功之后手动发送 ack 给 rabbitmq，rabbitmq 收到 ack 之后再从队列删除消息
+ 如果开启了 ack，消费了消息不发送 ack，将会内存泄漏。消息一直在消费，也不会删除

###### 配置文件

```yaml
spring:
  application:
    name: rabbitmq
  rabbitmq:
    host: 172.16.22.33
    port: 5672
    username: admin
    password: admin
```

###### 简单模式 - simple

一个生产者P发送消息到队列Q,一个消费者C接收

+ 创建队列

```java
@Configuration
public class RabbitQueueConfig {
    /**
     * 简单模式, 只需配置队列即可
     */
    @Bean
    public Queue queue(){
        return new Queue("q_simple");
    }
}
```

+ 消息生产

```java
@Component
public class SimpleProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void send(Map<String, Object> map){
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        map.put("date", date);
        // 发送消息到 q_simple 这个队列
        this.rabbitTemplate.convertAndSend("q_simple", map);
    }
}
```

+ 消息消费
    * 使用注解 @RabbitListener(queues = "q_simple") 标志为消息消费端, queues = "q_simple" 表示监听 q_simple 这个队列, 队列有消息过来触发

```java
@RabbitListener(queues = "q_simple")
@Component
public class SimpleConsumer {

    @RabbitHandler
    public void handle(Object o){
        // 对象转 Map
        BeanMap beanMap = new BeanMap(o);
        System.out.println(beanMap.toString());
    }

}
```

+ 测试

```java
@RestController
@RequestMapping("/simple")
public class SimpleController {

    @Resource
    private SimpleProducer producer;

    @GetMapping("/")
    public String sendMsg(){
        HashMap<String, Object> map = new HashMap<>(3);
        map.put("name", "IPhone XS");
        map.put("price", new BigDecimal("6299.99"));
        producer.send(map);
        return "消息发送到队列成功";
    }
}
```

###### 广播模式 - fanout

不需要路由键, 消息直接发送到交换机, 交换机会把消息转发到这台交换机的所有队列

+ 创建队列和交换机, fanout 类型的交换机

```java
@Configuration
public class RabbitQueueConfig {
    // 创建两个队列
    @Bean
    public Queue fanoutQueue0(){
        return new Queue("q_fanout_0");
    }
    @Bean
    public Queue fanoutQueue1(){
        return new Queue("q_fanout_1");
    }
    // 创建交换机
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("test_fanout_exchange");
    }
    // 两个队列和交换机进行绑定
    @Bean
    public Binding bindingFanoutQueue0FanoutExchange(){
        return BindingBuilder.bind(this.fanoutQueue0()).to(this.fanoutExchange());
    }
    @Bean
    public Binding bindingFanoutQueue1FanoutExchange(){
        return BindingBuilder.bind(this.fanoutQueue1()).to(this.fanoutExchange());
    }
}
```

+ 消息生产

```java
/**
 * fanout 模式消息生产者
 */
@Component
public class FanoutProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;

    /**
     * 发送消息到 test_fanout_exchange 交换机, 交换机会把消息转达到这台交换机的所有队列
     */
    public void send(Map<String, Object> map){
        this.rabbitTemplate.convertAndSend("test_fanout_exchange", null, map);
    }
}
```

+ 消息消费
    * 创建两个消费端, 测试是否都收到消息

```java
/**
 * 消费端 1
 */
@RabbitListener(queues = "q_fanout_0") // 监听 q_fanout_0 队列
@Component
public class FanoutConsumer1 {
    @RabbitHandler
    public void handle(Object o){
        // 对象转 Map
        BeanMap beanMap = new BeanMap(o);
        System.out.println(beanMap.toString());
    }
}
/**
 * 消费端 2
 */
@RabbitListener(queues = "q_fanout_1")  // 监听 q_fanout_1 队列
@Component
public class FanoutConsumer2 {
    @RabbitHandler
    public void handle(Object o){
        // 对象转 Map
        BeanMap beanMap = new BeanMap(o);
        System.out.println(beanMap.toString());
    }
}
```

+ 测试

```java
@RestController
@RequestMapping("/fanout")
public class FanoutController {

    @Resource
    private FanoutProducer producer;

    @GetMapping("/")
    public String send(){
        HashMap<String, Object> map = new HashMap<>(3);
        map.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        map.put("name", "MI 9 Pro");
        map.put("price", new BigDecimal("5555.555"));
        producer.send(map);
        return "消息发送成功";
    }
}
```

###### 主题模式 - topic

消息带上路由键发送到交换机, 交换机会根据路由键发送到指定的队列

+ 创建队列和交换机, topic 类型的交换

```java
@Configuration
public class RabbitQueueConfig {
    // 创建两个队列
    @Bean
    public Queue topicQueue0(){
        return new Queue("q_topic_0");
    }
    @Bean
    public Queue topicQueue1(){
        return new Queue("q_topic_1");
    }
    // 创建交换机
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange("test_topic_exchange");
    }
    // 交换机和队列进行绑定, 并为每个队列配置不同的路由键
    @Bean
    public Binding bindingTopicQueue0TopicExchange(){
        return BindingBuilder.bind(this.topicQueue0()).to(this.topicExchange()).with("topic.abc");
    }
    @Bean
    public Binding bindingTopicQueue1TopicExchange(){
        return BindingBuilder.bind(this.topicQueue1()).to(this.topicExchange()).with("topic.#");
    }
}
```

+ 消息生产

```java
/**
 * topic 模式消息生产者
 */
@Component
public class TopicProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;

    /**
     * 发送消息到 test_topic_exchange 交换机, 路由键为: topic.abc
     */
    public void send(Map<String, Object> map){
        this.rabbitTemplate.convertAndSend("test_topic_exchange", "topic.abc", map);
    }

    /**
     * 发送消息到 test_topic_exchange 交换机, 路由键为: topic.qwer
     */
    public void send2(Map<String, Object> map){
        this.rabbitTemplate.convertAndSend("test_topic_exchange", "topic.qwer", map);
    }
}
```

+ 消息消费
    * 两个消费者监听不同的队列

```java
/**
 * 消费者 1
 */
@RabbitListener(queues = "q_topic_0")   // 监听 q_topic_0 这个队列
@Component
public class TopicConsumer1 {

    @RabbitHandler
    public void handle(Object o){
        // 对象转 Map
        BeanMap beanMap = new BeanMap(o);
        System.out.println(beanMap.toString());
    }

}
/**
 * 消费者 2
 */
@RabbitListener(queues = "q_topic_1")   // 监听 q_topic_1 这个队列
@Component
public class TopicConsumer2 {

    @RabbitHandler
    public void handle(Object o){
        // 对象转 Map
        BeanMap beanMap = new BeanMap(o);
        System.out.println(beanMap.toString());
    }
}
```

+ 测试

```java
@RestController
@RequestMapping("/topic")
public class TopicController {

    @Resource
    private TopicProducer producer;

    /**
     * 使用精确路由键
     */
    @GetMapping("/s1")
    public String send1(){
        HashMap<String, Object> map = new HashMap<>(3);
        map.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        map.put("name", "HUAWEI Meta 30 Pro");
        map.put("price", new BigDecimal("9999.9999"));
        producer.send(map);
        return "消息发送成功";
    }

    /**
     * 使用 # 匹配路由键
     */
    @GetMapping("/s2")
    public String send2(){
        HashMap<String, Object> map = new HashMap<>(3);
        map.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        map.put("name", "HUAWEI Meta 20 Pro");
        map.put("price", new BigDecimal("8888.8888"));
        producer.send2(map);
        return "消息发送成功";
    }
}
```

###### 死信队列

+ 保证消息传达的及时性(定时器只能指定死的某个时期循环执行, 时间不能是动态的)
+ 消息先发送到死信交换机的死信队列并设置一个过期时间, 死信队列不会被消费, 达到过期时间把消息转发到替补队列(普通的消息队列, 并使用消费者监听)
+ 创建死信交换机/死信队列/替补队列

```java
@Configuration
public class RabbitQueueConfig {
    /**
     * 死信交换机
     */
    @Bean
    public Exchange deadExchange() {
        return ExchangeBuilder.directExchange("test_dead_exchange").durable(true).build();
    }
    /**
     * 死信队列
     *  x-dead-letter-exchange 指定死信交换机 test_dead_exchange
     *  x-dead-letter-routing-key 指定路由键 dead.abc
     */
    @Bean
    public Queue deadQueue() {
        Map<String, Object> args = new HashMap<>(2);
        args.put("x-dead-letter-exchange", "test_dead_exchange");
        args.put("x-dead-letter-routing-key", "dead.redirect");
        return QueueBuilder.durable("q_dead").withArguments(args).build();
    }
    /**
     * 替补队列, 死信队列的消息最后会放入这里
     */
    @Bean
    public Queue redirectQueue() {
        return QueueBuilder.durable("q_redirect").build();
    }
    /**
     * 绑定死信队列和死信交换机
     *  队列: q_dead
     *  交换机: test_dead_exchange
     *  路由键: dead.dead
     */
    @Bean
    public Binding deadBinding() {
        return new Binding("q_dead", Binding.DestinationType.QUEUE, "test_dead_exchange", "dead.dead", null);

    }
    /**
     * 绑定替补队列和死信交换机
     *  队列: q_redirect
     *  交换机: test_dead_exchange
     *  路由键: dead.redirect
     */
    @Bean
    public Binding redirectBinding() {
        return new Binding("q_redirect", Binding.DestinationType.QUEUE, "test_dead_exchange", "dead.redirect", null);
    }
}
```

+ 消息生产, 带上过期时间发送到死信队列

```java
@Component
public class DeadProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void send(String msg){
        // 发送消息到死信队列 q_dead
        this.rabbitTemplate.convertAndSend("test_dead_exchange", "dead.dead", msg, item -> {
            item.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            // 模拟，设置10S后消息过期
            item.getMessageProperties().setExpiration("10000");
            return item;
        });
    }
}
```

+ 消息消费, 只监听替补队列, 死信队列不监听

```java
/**
 * 监听 q_redirect 替补队列
 */
@RabbitListener(queues = "q_redirect")
@Component
public class DeadConsumer {

    @RabbitHandler
    public void handle(Object o){
        System.out.println(o.toString());
    }

}
```

+ 测试

```java
@RestController
@RequestMapping("/dead")
public class DeadControll {

    @Resource
    private DeadProducer producer;

    @GetMapping("/")
    public String send(){
        producer.send("123456");
        return "消息发送成功";
    }
}
```
