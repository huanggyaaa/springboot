package com.study.rabbitmq.simple;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 简单模式, 队列配置
 *
 * @author huang guang yue
 * @version v1.0.0
 * @date 2020 2020/11/16 16:51
 */
@Configuration
public class SimpleQueueConfig {

    /**
     * 创建队列，名称：queue_simple
     */
    @Bean
    public Queue queue() {
        return new Queue("queue_simple");
    }
}
