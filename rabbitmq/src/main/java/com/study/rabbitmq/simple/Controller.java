package com.study.rabbitmq.simple;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 控制器，用于在页面发送消息
 *
 * @author huang guang yue
 * @version v1.0.0
 * @date 2020 2020/11/16 17:17
 */
@RestController
public class Controller {

    @Resource
    private Producer producer;

    @GetMapping("/simple")
    public String send(@RequestParam String msg) {
        producer.send(msg);
        return "success";
    }
}
