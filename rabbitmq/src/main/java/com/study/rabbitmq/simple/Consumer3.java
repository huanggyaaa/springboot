package com.study.rabbitmq.simple;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费者
 *
 * @author huang guang yue
 * @version v1.0.0
 * @date 2020 2020/11/16 17:25
 */
@Component
@RabbitListener(queues = "queue_simple")
public class Consumer3 {

    /**
     * 处理消息
     *
     * @param o 消息体
     */
    @RabbitHandler
    public void handle(String o) {
        System.out.println("Consumer3 收到消息：" + o);
    }
}
