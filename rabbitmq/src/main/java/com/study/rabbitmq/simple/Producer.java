package com.study.rabbitmq.simple;

import com.alibaba.fastjson.JSONObject;
import com.study.rabbitmq.simple.config.RabbitConfirmAndReturnConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * 生产者
 *
 * @author huang guang yue
 * @version v1.0.0
 * @date 2020 2020/11/16 17:01
 */
@Component
public class Producer {

    /**
     * 自动注入 confirm 和 return 机制
     */
    @Resource
    private RabbitConfirmAndReturnConfig confirmAndReturnConfig;

    /**
     * 构造注入 template，方便设置 confirm 和 return 机制
     */
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public Producer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;

        // 消息发送失败重新回到队列
        rabbitTemplate.setMandatory(true);
        // 指定 confirm 机制
        rabbitTemplate.setConfirmCallback(confirmAndReturnConfig);
        // 指定 return 机制
        rabbitTemplate.setReturnCallback(confirmAndReturnConfig);
    }

    /**
     * 发送消息
     *
     * @param msg 消息
     */
    public void send(String msg) {

        // 消息对象
        HashMap<String, Object> map = new HashMap<String, Object>(4) {{
            put("msg", msg);
            put("sendTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        }};

        // 发送消息，第三个参数指定唯一消息标识
        rabbitTemplate.convertAndSend("queue_simple", JSONObject.toJSONString(map));
    }

}
