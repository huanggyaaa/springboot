//package com.study.rabbitmq.simple;
//
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.connection.CorrelationData;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Map;
//
///**
// * 简单模式消息生产者
// * @author huang guang yue
// */
//@Component
//public class SimpleProducer implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback {
//
//    /**
//     * 构造注入，同时设置回调
//     */
//    private final RabbitTemplate rabbitTemplate;
//    @Autowired
//    public SimpleProducer(RabbitTemplate rabbitTemplate){
//        this.rabbitTemplate = rabbitTemplate;
//        // 设置消息到达 exchange 时，要回调的方法，每个 RabbitTemplate 只支持一个 ConfirmCallback
//        rabbitTemplate.setConfirmCallback(this);
//        // 设置消息无法到达 queue 时，要回调的方法
//        rabbitTemplate.setReturnCallback(this);
//    }
//
//    public void send(Map<String, Object> map){
//        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
//        map.put("date", date);
//        // 发送消息到 q_simple 这个队列, 为了保证消息的可靠性, 必须构建 CorrelationData 对象, 该对象只有一个 ID 属性, 用来确保消息唯一性
//        this.rabbitTemplate.convertAndSend("q_simple", map, new CorrelationData("123"));
//    }
//
//    /**
//     * 监听消息是否到达 exchange
//     *
//     * @param correlationData 包含消息的唯一标识对象
//     * @param b true 标识 ack，false 标识 nack
//     * @param s nack 的原因
//     */
//    @Override
//    public void confirm(CorrelationData correlationData, boolean b, String s) {
//        String id = correlationData.getId();
//        if (b){
//            System.out.println("消息到达 exchange, 消息 ID : "+id);
//        } else {
//            System.out.println("消息没有到达 exchange, 消息 ID : "+id);
//        }
//    }
//
//    /**
//     * 监听消息是否到达 queue
//     *  消息已经到达但无法到达 queue (比如 exchange 找不到跟 routingKey 对应的 queue)
//     *
//     * @param message 返回的消息
//     * @param i 回复 code
//     * @param s 回复 text
//     * @param s1 交换机
//     * @param s2 路由键
//     */
//    @Override
//    public void returnedMessage(Message message, int i, String s, String s1, String s2) {
//
//        // correlationId 就是发消息时设置的 id
//        String correlationId = message.getMessageProperties().getHeaders().get("spring_returned_message_correlation").toString();
//
//        System.out.println("没有找到对应队列，消息投递失败,ID为: "+ correlationId +
//                        ", replyCode "+i+
//                        " , replyText "+s+
//                        ", exchange "+s1+
//                        " routingKey "+s2);
//
//    }
//}
