//package com.study.rabbitmq.simple;
//
//import org.apache.commons.beanutils.BeanMap;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
///**
// * 监听 q_simple 这个队列
// */
//@RabbitListener(queues = "q_simple")
//@Component
//public class SimpleConsumer {
//
//    @RabbitHandler
//    public void handle(Object o){
//        // 对象转 Map
//        BeanMap beanMap = new BeanMap(o);
//        System.out.println(beanMap.toString());
//    }
//
//}
