package com.study.rabbitmq.simple.config;

import com.alibaba.fastjson.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * 消息 confirm 和 return 机制处理
 *
 * @author huang guang yue
 * @version v1.0.0
 * @date 2020 2020/11/17 11:36
 */
@Component
public class RabbitConfirmAndReturnConfig implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnCallback {

    /**
     * confirm 机制
     *
     * @param correlationData 消息 ID
     * @param b               ack
     * @param s               原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean b, String s) {
        if (!b) {
            System.out.println("消息ID：" + correlationData.getId() + "没有发送到交换机, 失败原因：" + s);
        }
    }

    /**
     * return 机制
     *
     * @param message 消息
     * @param i       响应code
     * @param s       响应内容
     * @param s1      交换机
     * @param s2      队列/routingKey
     */
    @Override
    public void returnedMessage(Message message, int i, String s, String s1, String s2) {
        System.out.println("消息没有成功投递到队列，消息：" + JSONObject.toJSONString(message) + "，响应code：" + i + "，响应内容：" + s + "，交换机：" + s1 + "，队列/routingKey：" + s2);
    }
}
