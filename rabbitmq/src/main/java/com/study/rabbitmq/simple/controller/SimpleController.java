//package com.study.rabbitmq.simple.controller;
//
//import com.study.rabbitmq.simple.SimpleProducer;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import java.math.BigDecimal;
//import java.util.HashMap;
//
//@RestController
//@RequestMapping("/simple")
//public class SimpleController {
//
//    @Resource
//    private SimpleProducer producer;
//
//    @GetMapping("/")
//    public String sendMsg(){
//        HashMap<String, Object> map = new HashMap<>(3);
//        map.put("name", "IPhone XS");
//        map.put("price", new BigDecimal("6299.99"));
//        producer.send(map);
//        return "消息发送到队列成功";
//    }
//}
