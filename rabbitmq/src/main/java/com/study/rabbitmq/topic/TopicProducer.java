//package com.study.rabbitmq.topic;
//
//import org.springframework.amqp.core.AmqpTemplate;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Map;
//
///**
// * topic 模式消息生产者
// */
//@Component
//public class TopicProducer {
//
//    @Resource
//    private AmqpTemplate rabbitTemplate;
//
//    /**
//     * 发送消息到 test_topic_exchange 交换机, 路由键为: topic.abc
//     */
//    public void send(Map<String, Object> map){
//        this.rabbitTemplate.convertAndSend("test_topic_exchange", "topic.abc", map);
//    }
//
//    /**
//     * 发送消息到 test_topic_exchange 交换机, 路由键为: topic.qwer
//     */
//    public void send2(Map<String, Object> map){
//        this.rabbitTemplate.convertAndSend("test_topic_exchange", "topic.qwer", map);
//    }
//}
