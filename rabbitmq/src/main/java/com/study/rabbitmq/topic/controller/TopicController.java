//package com.study.rabbitmq.topic.controller;
//
//import com.study.rabbitmq.topic.TopicProducer;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import java.math.BigDecimal;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//
//@RestController
//@RequestMapping("/topic")
//public class TopicController {
//
//    @Resource
//    private TopicProducer producer;
//
//    /**
//     * 使用精确路由键
//     */
//    @GetMapping("/s1")
//    public String send1(){
//        HashMap<String, Object> map = new HashMap<>(3);
//        map.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//        map.put("name", "HUAWEI Meta 30 Pro");
//        map.put("price", new BigDecimal("9999.9999"));
//        producer.send(map);
//        return "消息发送成功";
//    }
//
//    /**
//     * 使用 # 匹配路由键
//     */
//    @GetMapping("/s2")
//    public String send2(){
//        HashMap<String, Object> map = new HashMap<>(3);
//        map.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//        map.put("name", "HUAWEI Meta 20 Pro");
//        map.put("price", new BigDecimal("8888.8888"));
//        producer.send2(map);
//        return "消息发送成功";
//    }
//}
