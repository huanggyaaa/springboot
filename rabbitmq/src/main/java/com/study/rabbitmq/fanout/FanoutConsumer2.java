//package com.study.rabbitmq.fanout;
//
//import org.apache.commons.beanutils.BeanMap;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
///**
// * 监听 q_fanout_1 这个队列
// */
//@RabbitListener(queues = "q_fanout_1")
//@Component
//public class FanoutConsumer2 {
//
//    @RabbitHandler
//    public void handle(Object o){
//        // 对象转 Map
//        BeanMap beanMap = new BeanMap(o);
//        System.out.println(beanMap.toString());
//    }
//
//}
