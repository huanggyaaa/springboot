//package com.study.rabbitmq.fanout;
//
//import org.springframework.amqp.core.AmqpTemplate;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.util.Map;
//
///**
// * fanout 模式消息生产者
// */
//@Component
//public class FanoutProducer {
//
//    @Resource
//    private AmqpTemplate rabbitTemplate;
//
//    /**
//     * 发送消息到 test_fanout_exchange 交换机
//     */
//    public void send(Map<String, Object> map){
//        this.rabbitTemplate.convertAndSend("test_fanout_exchange", null, map);
//    }
//}
