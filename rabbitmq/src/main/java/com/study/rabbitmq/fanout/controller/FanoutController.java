//package com.study.rabbitmq.fanout.controller;
//
//import com.study.rabbitmq.fanout.FanoutProducer;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import java.math.BigDecimal;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//
//@RestController
//@RequestMapping("/fanout")
//public class FanoutController {
//
//    @Resource
//    private FanoutProducer producer;
//
//    @GetMapping("/")
//    public String send(){
//        HashMap<String, Object> map = new HashMap<>(3);
//        map.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//        map.put("name", "MI 9 Pro");
//        map.put("price", new BigDecimal("5555.555"));
//        producer.send(map);
//        return "消息发送成功";
//    }
//}
