//package com.study.rabbitmq.dead;
//
//import org.springframework.amqp.core.AmqpTemplate;
//import org.springframework.amqp.core.MessageDeliveryMode;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Map;
//
///**
// * 简单模式消息生产者
// */
//@Component
//public class DeadProducer {
//
//    @Resource
//    private AmqpTemplate rabbitTemplate;
//
//    public void send(String msg){
//        // 发送消息到死信队列 q_dead
//        this.rabbitTemplate.convertAndSend("test_dead_exchange", "dead.dead", msg, item -> {
//            item.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
//            // 模拟，设置10S后消息过期
//            item.getMessageProperties().setExpiration("10000");
//            return item;
//        });
//    }
//}
