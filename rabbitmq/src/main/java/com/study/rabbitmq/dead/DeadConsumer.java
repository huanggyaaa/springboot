//package com.study.rabbitmq.dead;
//
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
///**
// * 监听 q_redirect 替补队列
// */
//@RabbitListener(queues = "q_redirect")
//@Component
//public class DeadConsumer {
//
//    @RabbitHandler
//    public void handle(Object o){
//        System.out.println(o.toString());
//    }
//
//}
