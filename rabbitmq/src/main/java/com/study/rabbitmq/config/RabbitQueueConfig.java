//package com.study.rabbitmq.config;
//
//import org.springframework.amqp.core.*;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//public class RabbitQueueConfig {
//
//
//
//    /**
//     * 简单模式, 只需配置队列即可
//     */
//    @Bean
//    public Queue queue(){
//        return new Queue("q_simple");
//    }
//
//
//
//
//
//    /**
//     * topic 模式
//     *  1, 创建两个队列 q_topic_0, q_topic_1
//     *  2, 创建 topic 交换机, test_topic_exchange
//     *  3, 两个队列和交换机进行绑定, 路由键
//     */
//    @Bean
//    public Queue topicQueue0(){
//        return new Queue("q_topic_0");
//    }
//    @Bean
//    public Queue topicQueue1(){
//        return new Queue("q_topic_1");
//    }
//    @Bean
//    public TopicExchange topicExchange(){
//        return new TopicExchange("test_topic_exchange");
//    }
//    @Bean
//    public Binding bindingTopicQueue0TopicExchange(){
//        return BindingBuilder.bind(this.topicQueue0()).to(this.topicExchange()).with("topic.abc");
//    }
//    @Bean
//    public Binding bindingTopicQueue1TopicExchange(){
//        return BindingBuilder.bind(this.topicQueue1()).to(this.topicExchange()).with("topic.#");
//    }
//
//
//
//
//
//    /**
//     * fanout 模式
//     *  1, 创建两个队列 q_fanout_0, q_fanout_1
//     *  2, 创建 fanout 交换机, test_fanout_exchange
//     *  3, 两个队列和交换机进行绑定, 没有路由键
//     */
//    @Bean
//    public Queue fanoutQueue0(){
//        return new Queue("q_fanout_0");
//    }
//    @Bean
//    public Queue fanoutQueue1(){
//        return new Queue("q_fanout_1");
//    }
//    @Bean
//    public FanoutExchange fanoutExchange(){
//        return new FanoutExchange("test_fanout_exchange");
//    }
//    @Bean
//    public Binding bindingFanoutQueue0FanoutExchange(){
//        return BindingBuilder.bind(this.fanoutQueue0()).to(this.fanoutExchange());
//    }
//    @Bean
//    public Binding bindingFanoutQueue1FanoutExchange(){
//        return BindingBuilder.bind(this.fanoutQueue1()).to(this.fanoutExchange());
//    }
//
//
//    /**************************** 死信队列配置 ***************************/
//    /**
//     * 死信交换机
//     */
//    @Bean
//    public Exchange deadExchange() {
//        return ExchangeBuilder.directExchange("test_dead_exchange").durable(true).build();
//    }
//    /**
//     * 死信队列
//     *  x-dead-letter-exchange 指定死信交换机 test_dead_exchange
//     *  x-dead-letter-routing-key 指定路由键 dead.abc
//     */
//    @Bean
//    public Queue deadQueue() {
//        Map<String, Object> args = new HashMap<>(2);
//        args.put("x-dead-letter-exchange", "test_dead_exchange");
//        args.put("x-dead-letter-routing-key", "dead.redirect");
//        return QueueBuilder.durable("q_dead").withArguments(args).build();
//    }
//    /**
//     * 替补队列, 死信队列的消息最后会放入这里
//     */
//    @Bean
//    public Queue redirectQueue() {
//        return QueueBuilder.durable("q_redirect").build();
//    }
//    /**
//     * 绑定死信队列和死信交换机
//     *  队列: q_dead
//     *  交换机: test_dead_exchange
//     *  路由键: dead.dead
//     */
//    @Bean
//    public Binding deadBinding() {
//        return new Binding("q_dead", Binding.DestinationType.QUEUE, "test_dead_exchange", "dead.dead", null);
//
//    }
//    /**
//     * 绑定替补队列和死信交换机
//     *  队列: q_redirect
//     *  交换机: test_dead_exchange
//     *  路由键: dead.redirect
//     */
//    @Bean
//    public Binding redirectBinding() {
//        return new Binding("q_redirect", Binding.DestinationType.QUEUE, "test_dead_exchange", "dead.redirect", null);
//    }
//    /**************************** 死信队列配置 ***************************/
//}
