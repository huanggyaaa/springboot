package com.study.shiro.entity;

import lombok.Data;

/**
 * 用户登录实体
 *
 * @author huanggy
 * @date 2020/2/10 12:39
 */
@Data
public class UserVo {
    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;
}
