package com.study.shiro.mapper;

import com.study.shiro.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    @Select("select * from t_user where username = #{username}")
    User selectByUsername(@Param("username") String username);

    @Select("select * from t_user where username = #{username} and password = #{password}")
    User selectByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    User selectAllInfoByUserId(@Param("userId") Integer userId);

    User selectAllInfoByUsername(@Param("username") String username);


}