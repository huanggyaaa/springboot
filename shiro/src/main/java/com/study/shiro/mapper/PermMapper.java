package com.study.shiro.mapper;

import com.study.shiro.entity.Perm;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Perm record);

    int insertSelective(Perm record);

    Perm selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Perm record);

    int updateByPrimaryKey(Perm record);

    /**
     * 根据角色 ID 查询权限
     *
     * @param roleId 角色 ID
     */
    @Select("SELECT * FROM t_permission WHERE id in( SELECT perm_id FROM t_role_perm WHERE role_id = #{roleId})")
    List<Perm> selectByRoleId(@Param("roleId") Integer roleId);

    /**
     * 根据用户 ID 查询权限
     *
     * @param userId 用户 ID
     */
    @Select("SELECT * FROM t_permission WHERE id in( SELECT perm_id FROM t_role_perm WHERE role_id in (SELECT id FROM t_role WHERE id in( SELECT role_id FROM t_user_role WHERE user_id = #{userId})))")
    List<Perm> selectByUserId(@Param("userId") Integer userId);
}