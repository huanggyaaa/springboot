package com.study.shiro.mapper;

import com.study.shiro.entity.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    /**
     * 根据用户 ID 查询角色
     *
     * @param userId 用户 ID
     */
    @Select("SELECT * FROM t_role WHERE id in( SELECT role_id FROM t_user_role WHERE user_id = #{userId})")
    List<Role> selectByUserId(@Param("userId") Integer userId);

}