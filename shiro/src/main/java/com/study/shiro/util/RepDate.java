package com.study.shiro.util;

import lombok.Data;

import java.io.Serializable;

/**
 * 接口响应数据
 *
 * @author huanggy
 * @date 2020/2/10 12:05
 */
@Data
public class RepDate implements Serializable {
    private Integer code; // 状态码
    private Object data; // 数据
    private String msg; // 描述

    public RepDate() {
    }

    public RepDate(Integer code, Object data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    // 成功, 返回状态码
    public static RepDate success() {
        return new RepDate(1, null, null);
    }

    // 成功, 返回状态码和数据
    public static RepDate success(Object data) {
        return new RepDate(1, data, null);
    }

    // 成功, 返回状态码、数据和描述
    public static RepDate success(Object data, String msg) {
        return new RepDate(1, data, msg);
    }

    // 成功, 返回状态码和描述
    public static RepDate success(String msg) {
        return new RepDate(1, null, msg);
    }

    // 失败, 返回状态码和描述
    public static RepDate error(String msg) {
        return new RepDate(0, null, msg);
    }
}
