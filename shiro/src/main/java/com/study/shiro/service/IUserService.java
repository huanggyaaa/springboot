package com.study.shiro.service;

import com.study.shiro.entity.User;
import org.apache.ibatis.annotations.Param;

/**
 * description
 *
 * @author huanggy
 * @date 2020/2/9 16:53
 */
public interface IUserService {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    User selectByUsername(String username);

    User selectByUsernameAndPassword(String username, String password);

    User selectAllInfoByUserId(Integer userId);

    User selectAllInfoByUsername(String username);

}
