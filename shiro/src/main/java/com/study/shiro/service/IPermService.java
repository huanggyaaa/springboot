package com.study.shiro.service;

import com.study.shiro.entity.Perm;

import java.util.List;

/**
 * description
 *
 * @author huanggy
 * @date 2020/2/9 16:53
 */
public interface IPermService {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Perm record);

    Perm selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Perm record);

    List<Perm> selectByRoleId(Integer roleId);

    List<Perm> selectByUserId(Integer userId);
}
