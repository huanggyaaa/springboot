package com.study.shiro.service.impl;

import com.study.shiro.entity.Perm;
import com.study.shiro.mapper.PermMapper;
import com.study.shiro.service.IPermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * description
 *
 * @author huanggy
 * @date 2020/2/10 10:29
 */
@Service
public class PermServiceImpl implements IPermService {

    @Autowired
    private PermMapper permMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return permMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(Perm record) {
        return permMapper.insertSelective(record);
    }

    @Override
    public Perm selectByPrimaryKey(Integer id) {
        return permMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Perm record) {
        return permMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<Perm> selectByRoleId(Integer roleId) {
        return permMapper.selectByRoleId(roleId);
    }

    @Override
    public List<Perm> selectByUserId(Integer userId) {
        return permMapper.selectByUserId(userId);
    }
}
