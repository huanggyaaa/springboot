package com.study.shiro.service;

import com.study.shiro.entity.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * description
 *
 * @author huanggy
 * @date 2020/2/9 16:53
 */
public interface IRoleService {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    List<Role> selectByUserId(Integer userId);
}
