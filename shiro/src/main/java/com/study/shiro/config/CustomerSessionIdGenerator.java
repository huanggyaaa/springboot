package com.study.shiro.config;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;

import java.io.Serializable;
import java.util.UUID;

/**
 * 自定义生成 session_id
 *
 * @author huanggy
 * @date 2020/2/10 16:42
 */
public class CustomerSessionIdGenerator implements SessionIdGenerator {

    @Override
    public Serializable generateId(Session session) {

        // 使用 jwt
//        return JwtUtil.createJwt("huanggy1001");

        // 使用 UUID
        return "huanggy" + UUID.randomUUID().toString().replace("-", "");
    }

}
