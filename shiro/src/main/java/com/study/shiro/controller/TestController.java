package com.study.shiro.controller;

import com.study.shiro.service.IUserService;
import com.study.shiro.util.RepDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * 测试控制层
 *
 * @author huanggy
 * @date 2020/2/10 13:42
 */
@RestController
public class TestController {

    @Autowired
    private IUserService userService;

    /**
     * 查询用户基础信息, 需要登录
     */
    @GetMapping("/authc/user/{userId}")
    public RepDate getById(@PathVariable Integer userId) {
        return RepDate.success(userService.selectByPrimaryKey(userId), "请求成功");
    }

    /**
     * 查询用户全部信息, 包括角色和权限信息, 需要登录
     */
    @GetMapping("/authc/user/all/{userId}")
    public RepDate getAllUser(@PathVariable Integer userId) {
        return RepDate.success(userService.selectAllInfoByUserId(userId), "请求成功");
    }

    /**
     * 具有 order 权限才能访问
     */
    @GetMapping("/order/")
    public RepDate order() {
        ArrayList<String> list = new ArrayList<>();
        list.add("订单1");
        list.add("订单2");
        return RepDate.success(list, "请求成功");
    }

    /**
     * 具有 admin 角色才能访问
     */
    @GetMapping("/admin/")
    public RepDate admin() {
        return RepDate.success("具有 admin 角色", "请求成功");
    }

}
