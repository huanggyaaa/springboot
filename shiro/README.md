#### 认证流程(登录)

+ 构造 SecurityManager 环境: 根据用户名和密码进行构造
+ 调用 Subject.login() 执行认证: SecurityManager 进行认证
+ Authenticator 执行认证: 根据 Realm 进行验证

#### shiro 内置 filter 过滤器

+ authc: org.apache.shiro.web.filter.authc.FormAuthenticationFilter
    * 必须登录才能访问
+ user: org.apache.shiro.web.filter.authc.UserFilter
    * 和上面类似, 一般用上面的
+ anon: org.apache.shiro.web.filter.authc.AnonymousFilter
    * 不需要登录就能访问
+ roles: org.apache.shiro.web.filter.authc.RolesAuthorizationFilter
    * 必须登录的用户具有指定的角色才能访问
    * 参数可写多个, 表示必须拥有全部的角色才能访问 roles["admin,user"]
+ perms: org.apache.shiro.web.filter.authc.PermissionsAuthorizationFilter
    * 登录的用户必须拥有指定的权限才能访问
    * 参数可写多个, 表示必须拥有全部的权限才能访问 perms["add,delete"]

#### shiro 拦截路径配置

+ ?: 匹配一个字符, 如 /user?, 匹配 /user1、/users, 不匹配 /user/
+ *: 匹配任意个字符, 如 /user\*, 匹配 /user123、/user, 不匹配 /user/1
+ **: 任意个字符且多级匹配, 如 /add\**, 匹配 /add123/video/select

#### 权限控制

+ 注解方式, 使用在控制层的接口上
    * @RequiresRoles(value={"admin","editor"}, logical=Logical.AND), 同时具有 admin 和 editor 角色
    * @RequiresPermissions(value="user:add", "user:del", logical=Logical.OR), 具有 user:add 或者 user:del 其中一个权限即可
    * RequiresAuthentication, 登录才能访问
    * RequiresUser, 类似上面
+ 编程方式

```java
Subject subject = SecurityUtils.getSubject();
// 基于角色
if(subject.hasRole("admin")){
    // 有角色
} else {
    // 无角色
}

// 基于权限
if(subject.isPermitted("user:add")){
    // 有权限
} else {
    // 无权限
}
```