package com.study.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一响应
 *
 * @author huang guang yue
 * @version v1.0.0
 * @date 2020 2020/10/29 11:05
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("统一响应")
public class RestResult<T> {

    @ApiModelProperty("业务code, 0表示成功，非0表示出错")
    private Integer code;

    @ApiModelProperty("提示信息,供报错时使用")
    private String msg;

    @ApiModelProperty("返回的数据")
    private T data;

    public static <T> RestResult<T> success() {
        return new RestResult<>(0, "请求成功", null);
    }

    public static <T> RestResult<T> success(T data) {
        return new RestResult<>(0, "请求成功", data);
    }

    public static <T> RestResult<T> success(String msg) {
        return new RestResult<>(0, msg, null);
    }

    public static <T> RestResult<T> success(String msg, T t) {
        return new RestResult<>(0, msg, t);
    }

    public static <T> RestResult<T> error() {
        return new RestResult<>(1, "请求失败", null);
    }

    public static <T> RestResult<T> error(String msg) {
        return new RestResult<>(1, msg, null);
    }

    public static <T> RestResult<T> error(String msg, T t) {
        return new RestResult<>(1, msg, t);
    }
}
