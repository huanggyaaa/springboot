package com.study.common;

import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 *
 * @author huang guang yue
 * @version v1.0.0
 * @date 2020 2020/11/4 9:34
 */
@RestControllerAdvice
public class ControllerExceptionAdvice {

    /**
     * Hibernate 参数校验失败
     *
     * @param e MethodArgumentNotValidException
     * @return com.ws.project.transfer.project.xin.common.RestResult
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RestResult<Object> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        ObjectError objectError = e.getBindingResult().getAllErrors().get(0);
        return RestResult.error("参数校验失败", objectError.getDefaultMessage());
    }
}
