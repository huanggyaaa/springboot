package com.study.controller.product;

import com.study.common.RestResult;
import com.study.entity.Product;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huanggy
 * @date 2019/2/26 20:07
 **/
@RestController
@RequestMapping("/api/v1/product")
@Api("产品模块相关接口")
public class ProductController {

    private List<Product> list = new ArrayList<>();

    {
        list.add(new Product(10001L, "IPhoneX", 9999.99, "米国"));
        list.add(new Product(10002L, "IPhone 8 Plus", 7899.99, "米国"));
        list.add(new Product(10003L, "华为 Mate20", 5399.00, "中国"));
        list.add(new Product(10004L, "华为 Mate20 Pro", 6999.00, "中国"));
        list.add(new Product(10005L, "OPPO R17", 3999.00, "中国"));
        list.add(new Product(10006L, "OPPO FindX", 4999.99, "中国"));
        list.add(new Product(10007L, "VIVO X23", 3799.99, "中国"));
        list.add(new Product(10008L, "小米 8", 2099.00, "中国"));
        list.add(new Product(10009L, "小米 9", 3999.00, "中国"));
        list.add(new Product(10010L, "红米 Note7", 1199.00, "中国"));
        list.add(new Product(10011L, "三星 S9+", 6999.00, "韩国"));
        list.add(new Product(10012L, "三星 S8", 3599.00, "韩国"));
        list.add(new Product(10013L, "IPhone 7", 3999.00, "美国"));
        list.add(new Product(10014L, "IPhone 7 Plus", 4999.00, "美国"));
        list.add(new Product(10015L, "华为 Mate X", 16999.00, "中国"));
    }

    @ApiOperation(value = "根据产品 ID 获取产品信息")
    @GetMapping("/{id}")
    public RestResult<Product> getById(@PathVariable Long id) {
        for (Product product : list) {
            if (product.getId() == id + 10001L) {
                return RestResult.success(product);
            }
        }
        return RestResult.success("没有数据");
    }

    @ApiOperation(value = "添加")
    @PostMapping("/")
    public RestResult<Product> testPost(@RequestBody @Valid Product product) {
        return RestResult.success(product);
    }
}
