package com.study.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Author: huanggy
 * Date: 2019/2/26 21:40
 **/
@ApiModel("商品信息")
@Data
public class Product {

    @ApiModelProperty("产品 ID")
    @NotNull(message = "ID不能为空")
    private Long id;

    @ApiModelProperty("产品名称")
    @NotNull(message = "产品名称不能为空")
    private String productName;

    @ApiModelProperty("产品单价 ")
    private Double price;
    @ApiModelProperty("产品产地")
    private String addr;

    public Product(Long id, String productName, Double price, String addr) {
        this.id = id;
        this.productName = productName;
        this.price = price;
        this.addr = addr;
    }
}
