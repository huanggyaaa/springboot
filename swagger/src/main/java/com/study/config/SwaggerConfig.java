package com.study.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * swagger 配置
 *
 * @author huanggy
 * @date 2019/2/26 18:22
 **/

@Configuration
public class SwaggerConfig {

    // 允许匿名访问
    // /swagger**/**
    // /webjars/**
    // /v3/**
    // /doc.html

    /**
     * 流程拆解1.0
     */
    @Bean("model-product")
    public Docket disassembleFlowDocketV1() {
        return create("model-product",
                "com.study.controller.product");
    }

    /**
     * 流程拆解2.0，带通用请求参数
     */
    @Bean("model-user")
    public Docket disassembleFlowDocketV2() {
        return create("model-user",
                "com.study.controller.user")
                .globalOperationParameters(getGlobalRequestParameters());
    }

    /**
     * 甘特图
     */
    @Bean("gantt")
    public Docket ganttDocket() {
        return create("gantt",
                "com.ws.project.transfer.project.xin.gantt.controller");
    }

    /**
     * 生成接口信息，包括标题、联系人等
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("西南项目API接口，测试环境")
                .description("如有疑问请自行解决，因为我的代码没有 bug！！！！")
                .contact(new Contact("黄光跃", "https://gitee.com/huanggyaaa", "huanggyaaa@gmail.com"))
                .version("1.0")
                .build();
    }

    /**
     * 通用参数
     */
    private List<Parameter> getGlobalRequestParameters() {
        List<Parameter> parameters = new ArrayList<>();

        // 请求头 token 参数
        Parameter tokenParam = new ParameterBuilder()
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .name("token")
                .description("token")
                .required(false)
                .build();

        // 添加 token
        parameters.add(tokenParam);
        return parameters;
    }

    /**
     * PathSelectors.regex("/api.*")过滤规则表达式
     *
     * @param groupName   组名
     * @param basePackage 包路径
     * @return Docket
     */
    private Docket create(String groupName, String basePackage) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                .apiInfo(apiInfo())
                // 定义是否开启 swagger
                .enable(true)
                .select()
                // 生成文档的基础 package
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build()
                // 协议
                .protocols(new LinkedHashSet<>(Arrays.asList("http", "https")));
    }

}
